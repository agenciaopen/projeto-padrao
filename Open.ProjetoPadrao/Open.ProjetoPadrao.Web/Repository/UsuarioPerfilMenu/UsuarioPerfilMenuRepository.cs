﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class UsuarioPerfilMenuRepository : RepositoryBase<UsuarioPerfilMenu>, IUsuarioPerfilMenuRepository
  {
    private readonly Context _context;

    public UsuarioPerfilMenuRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Menu> BuscarMenusPerfil(Guid idUsuarioPerfil)
    {
      return _context.UsuarioPerfilMenus
        .Where(a => a.IDUsuarioPerfil.Equals(idUsuarioPerfil))
        .Include(a => a.Menu)
        .OrderBy(m => m.Menu.IDMenuPai).ThenBy(m => m.Menu.OrdemExibicao)
        .Select(b => b.Menu)
        .ToList();
    }

    public void RemoverMenusPerfil(Guid idUsuarioPerfil)
    {
      var menus = _context.UsuarioPerfilMenus
        .Where(a => a.IDUsuarioPerfil.Equals(idUsuarioPerfil));

      if (menus != null)
      {
        _context.UsuarioPerfilMenus.RemoveRange(menus);
        _context.SaveChanges();
      }
    }

    public void SalvarMenusPerfil(ICollection<UsuarioPerfilMenu> model)
    {
      _context.UsuarioPerfilMenus.AddRange(model);
      _context.SaveChanges();
    }
  }
}