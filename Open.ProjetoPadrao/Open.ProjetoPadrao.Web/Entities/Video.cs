﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Video")]
  public class Video
  {
    [Key]
    public Guid IDVideo { get; set; }

    [MaxLength(500)]
    public string URL { get; set; }

    [Required, MaxLength(500)]
    public string Titulo { get; set; }

    [MaxLength(500)]
    public string Descricao { get; set; }

    [Required, MaxLength(500)]
    public string Alt { get; set; }

    [Required]
    public bool ExibirHome { get; set; }

    [Required]
    public DateTime DataCriacao { get; set; }

    [Required]
    public int CodigoTipoVideo
    {
      get
      {
        return (int)TipoVideo;
      }
      set
      {
        TipoVideo = (TipoVideo)value;
      }
    }

    [EnumDataType(typeof(TipoVideo)), NotMapped]
    public TipoVideo TipoVideo { get; set; }
  }
}