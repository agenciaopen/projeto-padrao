﻿using System;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class PaginaControleParametroRepository : RepositoryBase<PaginaControleParametro>, IPaginaControleParametroRepository
  {
    private readonly Context _context;

    public PaginaControleParametroRepository(Context context) : base(context)
    {
      _context = context;
    }

    public void RemoverTodosParametrosControle(Guid idPaginaControle)
    {
      var parametros = _context.PaginasControleParametros
        .Where(a => a.IDPaginaControle.Equals(idPaginaControle))
        .ToList();

      if (parametros != null && parametros.Count > 0)
      {
        _context.PaginasControleParametros.RemoveRange(parametros);
        _context.SaveChanges();
      }
    }

    public void AdicionarParametrosControle(List<PaginaControleParametro> model)
    {
      _context.PaginasControleParametros.AddRange(model);
      _context.SaveChanges();
    }

    public IEnumerable<PaginaControleParametro> BuscarParametrosPagina(string identificadorPagina)
    {
      return _context.PaginasControleParametros
        .Where(a => a.PaginaControle.Pagina.Identificador.Equals(identificadorPagina, StringComparison.InvariantCultureIgnoreCase));
    }
  }
}