﻿using System;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public sealed class PaginaControleParametrosViewModel
  {
    public Guid IDPaginaControleParametro { get; set; }

    public Guid IDPaginaControle { get; set; }

    public string IdentificadorControle { get; set; }

    public string ConteudoTexto { get; set; }

    public string ConteudoHeadline { get; set; }
  }
}