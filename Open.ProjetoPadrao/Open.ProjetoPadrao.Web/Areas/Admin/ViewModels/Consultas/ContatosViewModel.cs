﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class ContatosViewModel
  {
    public Guid IDContato { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
    public string Celular { get; set; }

    public DateTime DataHoraInclusao { get; set; }
    public bool Visualizado { get; set; }
    public string Origem { get; set; }
    public int QtdAnexos { get; set; }
  }
}