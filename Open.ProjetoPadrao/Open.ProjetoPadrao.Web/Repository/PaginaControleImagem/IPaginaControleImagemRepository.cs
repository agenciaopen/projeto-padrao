﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IPaginaControleImagemRepository : IRepositoryBase<PaginaControleImagem>
  {
    void SalvarControleImagem(PaginaControleImagem model);
    PaginaControleImagem BuscarPaginaControleImagem(Guid idPaginaControleImagem);
    void RemoverPaginaControleImagem(PaginaControleImagem model);
    void RemoverControleImagens(Guid idPagina);
    IEnumerable<PaginaControleImagem> BuscarPaginaControleImagens(Guid idPaginaControle);
    void SalvarDetalhesPaginaControleImagem(Guid idPaginaControleImagem, string alt, string title, string legenda, string titulo, string subTitulo, string urlRedirecionamento, string altEN, string titleEN, string legendaEN, string tituloEN, string subTituloEN, string urlRedirecionamentoEN);
    void ReordenarImagens(Guid idPaginaControleImagem, int novaOrdemExibicao);
  }
}