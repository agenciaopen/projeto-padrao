﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Usuario")]
  public class Usuario
  {
    [Key]
    public Guid IDUsuario { get; set; }

    [Required]
    public Guid IDUsuarioPerfil { get; set; }

    [Required]
    public Guid IDPessoa { get; set; }

    [Required]
    [MaxLength(255)]
    public string Login { get; set; }

    [Required]
    [MaxLength(255)]
    public string Senha { get; set; }

    [Required]
    public bool Ativo { get; set; }

    [Required]
    public DateTime DataHoraCriacao { get; set; }

    public DateTime? DataHoraAlteracao { get; set; }

    public virtual UsuarioPerfil UsuarioPerfil { get; set; }

    public virtual Pessoa Pessoa { get; set; }
  }
}