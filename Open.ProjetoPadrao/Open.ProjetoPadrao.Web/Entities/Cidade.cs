﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Cidade")]
  public class Cidade
  {
    public Cidade()
    {
      this.Regioes = new Collection<Bairro>();
    }

    [Key]
    public Guid IDCidade { get; set; }

    [Required]
    public Guid IDEstado { get; set; }

    [Required]
    public string Nome { get; set; }


    public virtual Estado Estado { get; set; }

    public virtual ICollection<Bairro> Regioes { get; set; }
  }
}