﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class PaginaControleImagemRepository : RepositoryBase<PaginaControleImagem>, IPaginaControleImagemRepository
  {
    private readonly Context _context;

    public PaginaControleImagemRepository(Context context) : base(context)
    {
      _context = context;
    }

    public void SalvarControleImagem(PaginaControleImagem model)
    {
      var ordemExibicao = _context
        .PaginasControleImagens.Where(a => a.OrdemExibicao != null)
        .Max(a => a.OrdemExibicao.Value) + 1;

      model.OrdemExibicao = ordemExibicao;
      model.DataHoraInclusao = DateTime.Now;

      _context.PaginasControleImagens.Add(model);
      _context.SaveChanges();
    }

    public PaginaControleImagem BuscarPaginaControleImagem(Guid idPaginaControleImagem)
    {
      return _context.PaginasControleImagens
        .Include(a => a.Arquivo)
        .FirstOrDefault(a => a.IDPaginaControleImagem.Equals(idPaginaControleImagem));
    }

    public void RemoverPaginaControleImagem(PaginaControleImagem model)
    {
      _context.PaginasControleImagens.Remove(model);
      _context.SaveChanges();
    }

    public void RemoverControleImagens(Guid idPagina)
    {
      var todasImagens = _context.PaginasControleImagens
        .Where(a => a.PaginaControle.IDPagina == idPagina);

      if (todasImagens != null && todasImagens.Count() > 0)
      {
        _context.PaginasControleImagens.RemoveRange(todasImagens);
        _context.SaveChanges();
      }
    }

    public IEnumerable<PaginaControleImagem> BuscarPaginaControleImagens(Guid idPaginaControle)
    {
      return _context.PaginasControleImagens
        .Where(a => a.IDPaginaControle.Equals(idPaginaControle))
        .OrderBy(a => a.OrdemExibicao);
    }

    public void SalvarDetalhesPaginaControleImagem(Guid idPaginaControleImagem, string alt, string title, string legenda, string titulo, string subTitulo, string urlRedirecionamento, string altEN, string titleEN, string legendaEN, string tituloEN, string subTituloEN, string urlRedirecionamentoEN)
    {
      var model = _context.PaginasControleImagens
        .FirstOrDefault(a => a.IDPaginaControleImagem.Equals(idPaginaControleImagem));

      if (model != null)
      {
        model.Alt = string.IsNullOrEmpty(alt) ? "" : alt;
        model.Title = string.IsNullOrEmpty(title) ? "" : title;
        model.Legenda = string.IsNullOrEmpty(legenda) ? "" : legenda;
        model.TextoTitulo = string.IsNullOrEmpty(titulo) ? "" : titulo;
        model.TextoSubTitulo = string.IsNullOrEmpty(subTitulo) ? "" : subTitulo;
        model.URLRedirecionamento = string.IsNullOrEmpty(urlRedirecionamento) ? "" : urlRedirecionamento;

        model.AltEN = string.IsNullOrEmpty(altEN) ? "" : altEN;
        model.TitleEN = string.IsNullOrEmpty(titleEN) ? "" : title;
        model.LegendaEN = string.IsNullOrEmpty(legendaEN) ? "" : legendaEN;
        model.TextoTituloEN = string.IsNullOrEmpty(tituloEN) ? "" : tituloEN;
        model.TextoSubTituloEN = string.IsNullOrEmpty(subTituloEN) ? "" : subTituloEN;
        model.URLRedirecionamentoEN = string.IsNullOrEmpty(urlRedirecionamentoEN) ? "" : urlRedirecionamentoEN;

        _context.SaveChanges();
      }
    }

    public void ReordenarImagens(Guid idPaginaControleImagem, int novaOrdemExibicao)
    {
      var model = BuscarPaginaControleImagem(idPaginaControleImagem);

      if (model != null)
      {
        model.OrdemExibicao = novaOrdemExibicao;
        _context.SaveChanges();
      }
    }
  }
}