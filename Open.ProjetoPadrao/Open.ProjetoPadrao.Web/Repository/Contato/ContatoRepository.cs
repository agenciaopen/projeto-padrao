﻿using System;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Enums;
using System.Data.Entity;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class ContatoRepository : RepositoryBase<Contato>, IContatoRepository
  {
    private readonly Context _context;

    public ContatoRepository(Context context) : base(context)
    {
      _context = context;
    }

    public void CadastrarContato(Contato contato)
    {
      contato.DataHoraInclusao = DateTime.Now;
      this._context.Contatos.Add(contato);
      this._context.SaveChanges();
    }

    public Contato CarregarContato(Guid idContato)
    {
      return this._context.Contatos.Include(s => s.ListaContatoAnexo)
                                   .Include(s => s.ListaContatoAnexo.Select(a => a.Arquivo))
                                   .Include(s => s.Cidade.Estado) 
                                   .First(s => s.IDContato == idContato);
    }

    public IEnumerable<Contato> BuscarContatos()
    {
      return _context.Contatos
        .OrderByDescending(a => a.DataHoraInclusao)
        .ToList();
    }

    public IEnumerable<Contato> BuscarContatos(TipoContato tipo)
    {
      return Get()
        .Where(s => s.TipoContato.Equals(tipo))
        .OrderByDescending(a => a.DataHoraInclusao)
        .ToList();
    }

    public void RemoverContato(Guid idContato)
    {
      Contato contato = _context.Contatos.Where(a => a.IDContato.Equals(idContato)).FirstOrDefault();

      if (contato != null)
      {
        if (contato.ListaContatoAnexo.Count() > 0)
        {
          string caminhoArquivo = contato.ListaContatoAnexo.First().Arquivo.Caminho + contato.ListaContatoAnexo.First().Arquivo.NomeReal;
          new ArquivoRepository(_context).ExcluirListaArquivosDiretorio(contato.ListaContatoAnexo.Select(s => s.Arquivo).ToList());
        }
        _context.Contatos.Remove(contato);
        _context.SaveChanges();
      }
    }

    public void AtualizarContatoVisualizado(Guid idContato)
    {
      Contato contato = this._context.Contatos.Find(idContato);
      contato.Visualizado = true;

      this._context.Entry(contato).State = EntityState.Modified;
      this._context.SaveChanges();
    }
  }
}