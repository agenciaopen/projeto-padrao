﻿using System.Web.Mvc;
using System.Reflection;

namespace Open.ProjetoPadrao.Web.Attributes
{
  public class AjaxOnlyAttribute : ActionMethodSelectorAttribute
  {
    public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
    {
      return controllerContext.RequestContext.HttpContext.Request.IsAjaxRequest();
    }
  }
}