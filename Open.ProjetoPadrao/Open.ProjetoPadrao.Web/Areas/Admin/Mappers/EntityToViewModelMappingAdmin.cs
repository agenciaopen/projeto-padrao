using System.Linq;
using AutoMapper;
using Open.ProjetoPadrao.Web.Enums;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.ViewModels.Admin;
using Open.Framework.Extensoes;

namespace Open.ProjetoPadrao.Web.Mappers.Admin
{
  public class EntityToViewModelMappingAdmin : Profile
  {
    public EntityToViewModelMappingAdmin()
    {
      CreateMap<Pagina, PaginaViewModel>();

      CreateMap<PaginaControleParametro, PaginaControleParametrosViewModel>();

      CreateMap<Pagina, PaginasViewModel>()
        .ForMember(a => a.PaginaConfigurada, b => b.MapFrom(src => src.PaginaControles.All(x => x.Configurado)));

      CreateMap<UsuarioPerfil, UsuarioPerfisViewModel>()
       .ForMember(a => a.QtdUsuarios, b => b.MapFrom(src => src.Usuarios.Count))
       .ForMember(a => a.Administrador, b => b.MapFrom(src => src.SuperUser));

      CreateMap<Video, VideoViewModel>()
       .ForMember(a => a.TipoVideo, b => b.MapFrom(src => src.CodigoTipoVideo));

      CreateMap<Usuario, UsuarioViewModel>()
        .ForMember(a => a.Nome, b => b.MapFrom(src => src.Pessoa.NomeCompleto))
        .ForMember(a => a.Email, b => b.MapFrom(src => src.Pessoa.Email))
        .ForMember(a => a.Senha, opt => opt.Ignore())
        .ForMember(a => a.ConfirmacaoSenha, opt => opt.Ignore());

      CreateMap<UsuarioPerfil, UsuarioPerfilViewModel>()
        .ForMember(a => a.ListaMenusSelecionados, b => b.MapFrom(src => src.UsuarioPerfilMenus.Select(z => z.IDMenu).ToList()));

      CreateMap<Usuario, UsuariosViewModel>()
        .ForMember(a => a.NomePerfil, b => b.MapFrom(src => src.UsuarioPerfil.Nome))
        .ForMember(a => a.Nome, b => b.MapFrom(src => src.Pessoa.NomeCompleto))
        .ForMember(a => a.SuperUser, b => b.MapFrom(src => src.UsuarioPerfil.SuperUser))
        .ForMember(a => a.Email, b => b.MapFrom(src => src.Pessoa.Email));

      CreateMap<Contato, ContatosViewModel>()
        .ForMember(a => a.QtdAnexos, b => b.MapFrom(src => src.ListaContatoAnexo.Count));

      CreateMap<Bairro, BairrosViewModel>()
        .ForMember(a => a.NomeCidade, b => b.MapFrom(src => src.Cidade.Nome))
        .ForMember(a => a.NomeEstado, b => b.MapFrom(src => src.Cidade.Estado.NomeCompleto))
        .ForMember(a => a.QtdEnderecosVinculados, b => b.MapFrom(src => src.Enderecos.Count()));

      CreateMap<Bairro, BairroViewModel>()
        .ForMember(a => a.IDEstado, b => b.MapFrom(src => src.Cidade.IDEstado))
        .ForMember(a => a.ListaCidades, opt => opt.Ignore())
        .ForMember(a => a.ListaEstados, opt => opt.Ignore());

      CreateMap<Contato, ContatoViewModel>()
       .ForMember(a => a.ObjContato, opt => opt.Ignore())
       .ForMember(a => a.Origem, b => b.MapFrom(src =>
          src.TipoContato == TipoContato.FaleConosco ? "Fale Conosco" :
          src.TipoContato == TipoContato.VendaSeuTerreno ? "Venda seu Terreno" :
          src.TipoContato == TipoContato.Parceria ? "Seja Parceiro" :
          src.TipoContato == TipoContato.Contato ? "Contato/Empreendimento" :
          src.TipoContato == TipoContato.LigamosParaVoce ? "Ligamos para Você" :
          "Origem desconhecida"));

      CreateMap<Contato, ContatosViewModel>()
       .ForMember(a => a.Origem, b => b.MapFrom(src =>
         !string.IsNullOrEmpty(src.NomeEmpreendimento) ? src.NomeEmpreendimento :
          src.TipoContato == TipoContato.FaleConosco ? "Fale Conosco" :
          src.TipoContato == TipoContato.VendaSeuTerreno ? "Venda seu Terreno" :
          src.TipoContato == TipoContato.Parceria ? "Seja Parceiro" :
          src.TipoContato == TipoContato.Contato ? "Contato/Empreendimento" :
          src.TipoContato == TipoContato.LigamosParaVoce ? "Ligamos para Você" :
          "Origem desconhecida"));
    }
  }
}