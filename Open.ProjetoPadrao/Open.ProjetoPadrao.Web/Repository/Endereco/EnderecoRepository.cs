﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class EnderecoRepository : RepositoryBase<Endereco>, IEnderecoRepository
  {
    private readonly Context _context;

    public EnderecoRepository(Context context) : base(context)
    {
      _context = context;
    }

    public Endereco BuscarEndereco(Guid idEndereco)
    {
      return Get().FirstOrDefault(a => a.IDEndereco.Equals(idEndereco));
    }

    public void SalvarEndereco(Endereco model)
    {
      Edit(model, true);
    }
  }
}