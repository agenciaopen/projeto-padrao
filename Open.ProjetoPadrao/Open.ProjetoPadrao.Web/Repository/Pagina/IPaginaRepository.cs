﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IPaginaRepository : IRepositoryBase<Pagina>
  {
    IEnumerable<Pagina> BuscarPaginas();
    Pagina BuscarPagina(Guid idPagina);
    Pagina BuscarPagina(string nomeIdentificador);
    void CadastrarPagina(Pagina model);
    void SalvarPagina(Guid idPagina, string nomePagina);
    void RemoverPagina(Guid idPagina);
  }
}