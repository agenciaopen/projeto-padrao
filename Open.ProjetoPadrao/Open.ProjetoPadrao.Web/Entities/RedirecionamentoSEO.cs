﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("RedirecionamentoSEO")]
  public class RedirecionamentoSEO
  {
    [Key]
    public Guid IDRedirecionamento { get; set; }

    [Required]
    public string URLAntiga { get; set; }

    [Required]
    public string URLNova { get; set; }

    [Required]
    public DateTime DataCadastro { get; set; }
  }
}