﻿$(document).on("click", "[data-id='btnRemoverVideo']", function () {

  var $this = $(this);
  var idVideo = $this.data("idvideo");
  var url = $("#urlRemoverVideo").val();
  
  swal({
    text: "Deseja remover este vídeo?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {

        $this.button('loading');

        $.ajax({
          url: url,
          type: "POST",
          data: { idVideo: idVideo },
          success: function (partial) {
            $this.button('reset');
            $("#divVideos").html(partial);
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirMensagem("E", "Ocorreu um erro ao remover o vídeo");
          }
        });
      }
    });

});