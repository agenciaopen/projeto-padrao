﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface ICidadeRepository : IRepositoryBase<Cidade>
  {
    IEnumerable<Cidade> BuscarCidades(Guid idEstado);
    Cidade ObterCidade(Guid idCidade);
  }
}
