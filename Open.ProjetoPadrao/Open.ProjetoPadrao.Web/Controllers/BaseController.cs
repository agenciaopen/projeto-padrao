﻿using AutoMapper;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.ViewModels.Site;
using Open.ProjetoPadrao.Web.Enums;
using Open.ProjetoPadrao.Web.Properties;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.Attributes;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Collections.Generic;

namespace Open.ProjetoPadrao.Web.Controllers
{
  [Localization("pt-BR")]
  public abstract class BaseController : Areas.Admin.Controllers.BaseController
  {
    public BaseController(IUnitOfWork unitOfWork) : base(unitOfWork) { }

    internal Pagina CarregarPagina(string identificadorPagina)
    {
      return _unitOfWork.PaginaRepository.BuscarPagina(identificadorPagina);
    }
    internal VitrineViewModel CarregarVitrine(Pagina pagina, string nomeIdentificadorControle)
    {
      if (pagina != null)
      {
        if (pagina.PaginaControles != null && pagina.PaginaControles.Count > 0)
        {
          PaginaControle galeriaControle = pagina.PaginaControles.FirstOrDefault(s => !string.IsNullOrEmpty(s.NomeIdentificadorControle) &&
          s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));

          if (galeriaControle != null)
          {
            PaginaControleImagem galeriaParametro = galeriaControle.PaginaControleImagens.FirstOrDefault();
            return Mapper.Map<PaginaControleImagem, VitrineViewModel>(galeriaParametro);
          }
        }
      }

      return new VitrineViewModel();
    }
    internal string CarregarTexto(Pagina pagina, string nomeIdentificadorControle, Idioma idioma)
    {
      string paginaParametro = string.Empty;

      if (pagina != null)
      {
        PaginaControle paginaControle = pagina.PaginaControles.FirstOrDefault(s => !string.IsNullOrEmpty(s.NomeIdentificadorControle) &&
        s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));

        if (paginaControle != null)
        {
          if (idioma == Idioma.pt)
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoTexto;
          }
          else
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoTextoEN;
          }
        }
      }

      return paginaParametro;
    }
    internal string CarregarTitle(Pagina pagina, string nomeIdentificadorControle, Idioma idioma)
    {
      string paginaParametro = string.Empty;

      if (pagina != null)
      {
        PaginaControle paginaControle = pagina.PaginaControles.FirstOrDefault(s => !string.IsNullOrEmpty(s.NomeIdentificadorControle) &&
        s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));

        if (paginaControle != null)
        {
          if (idioma == Idioma.pt)
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault() == null ? string.Empty :
              paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoTitle;
          }
          else
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault() == null ? string.Empty :
              paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoTitleEN;
          }
        }
      }

      return paginaParametro;
    }
    internal string CarregarDescription(Pagina pagina, string nomeIdentificadorControle, Idioma idioma)
    {
      string paginaParametro = string.Empty;

      if (pagina != null)
      {
        PaginaControle paginaControle = pagina.PaginaControles.FirstOrDefault(s => !string.IsNullOrEmpty(s.NomeIdentificadorControle) &&
        s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));

        if (paginaControle != null)
        {
          if (idioma == Idioma.pt)
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault() == null ? string.Empty :
              paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoDescription;
          }
          else
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault() == null ? string.Empty :
              paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoDescriptionEN;
          }
        }
      }

      return paginaParametro;
    }
    internal string CarregarHead(Pagina pagina, string nomeIdentificadorControle, Idioma idioma)
    {
      string paginaParametro = string.Empty;

      if (pagina != null)
      {
        PaginaControle paginaControle = pagina.PaginaControles.FirstOrDefault(s => !string.IsNullOrEmpty(s.NomeIdentificadorControle) &&
        s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));

        if (paginaControle != null)
        {
          if (idioma.Equals(Idioma.pt))
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoHeadline;
          }
          else
          {
            paginaParametro = paginaControle.PaginaControleParametros.FirstOrDefault().ConteudoHeadlineEN;
          }
        }
      }

      return paginaParametro;
    }
    internal IEnumerable<VitrineViewModel> CarregarGaleria(Pagina pagina, string nomeIdentificadorControle)
    {
      if (pagina != null)
      {
        PaginaControle galeriaControle = pagina.PaginaControles.FirstOrDefault(s => s.NomeIdentificadorControle.Equals(nomeIdentificadorControle));
        if (galeriaControle != null)
        {
          IEnumerable<PaginaControleImagem> galeriaParametro = galeriaControle.PaginaControleImagens;
          return galeriaParametro != null ? Mapper.Map<IEnumerable<VitrineViewModel>>(galeriaParametro) : Enumerable.Empty<VitrineViewModel>();
        }
      }

      return Enumerable.Empty<VitrineViewModel>();
    }
    internal void EnviarEmail(string nomeEmail, string conteudo, string responderPara)
    {
      #if DEBUG
        ConfiguracaoEmail.EnviarEmail(nomeEmail, conteudo, responderPara, new string[] { Configs.MailTo });
      #else
        ConfiguracaoEmail.EnviarEmail(nomeEmail, conteudo, responderPara, new string[] { Configs.MailTo });
      #endif
    }
    internal string BuscarModelo(TipoEmail tipo)
    {
      string retorno = string.Empty;

      switch (tipo)
      {
        case TipoEmail.Contato:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-contato.html"));
          break;
        case TipoEmail.Empreendimento:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-empreendimento.html"));
          break;
        case TipoEmail.FaleConosco:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-faleconosco.html"));
          break;
        case TipoEmail.Parceria:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-parceria.html"));
          break;
        case TipoEmail.LigamosParaVoce:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-ligamos.html"));
          break;
        case TipoEmail.VendaSeuTerreno:
          retorno = System.IO.File.ReadAllText(Server.MapPath("~/Email/modelo-vendaseuterreno.html"));
          break;
      }

      return retorno;
    }
    internal Idioma BuscarIdiomaAtual()
    {
      CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;

      if (currentCulture.Name.Equals(Configs.Portugues))
        return Idioma.pt;
      else return Idioma.en;
    }
  }
}