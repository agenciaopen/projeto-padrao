﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
  {
    private readonly Context _context;

    public UsuarioRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Usuario> BuscarUsuarios()
    {
      return _context.Usuarios
        .OrderBy(a => a.Pessoa.NomeCompleto);
    }

    public Usuario BuscarUsuario(string login)
    {
      return _context.Usuarios
        .Where(a => a.Login.Equals(login, StringComparison.InvariantCultureIgnoreCase))
        .FirstOrDefault();
    }

    public Usuario BuscarUsuario(Guid idUsuario)
    {
      return _context.Usuarios
       .Where(a => a.IDUsuario.Equals(idUsuario))
       .FirstOrDefault();
    }

    public Usuario RetornaUsuarioPessoa(Guid idUsuario)
    {
      return _context.Usuarios
        .Where(o => o.IDUsuario == idUsuario)
        .First();
    }

    public void CadastrarUsuario(Usuario model)
    {
      Add(model, true);
    }

    public void EditarUsuario(Usuario model)
    {
      var usuario = _context.Usuarios
        .FirstOrDefault(a => a.IDUsuario.Equals(model.IDUsuario));

      if (usuario != null)
      {
        usuario.Ativo = model.Ativo;
        usuario.IDUsuarioPerfil = model.IDUsuarioPerfil;

        if (!string.IsNullOrEmpty(model.Senha))
          usuario.Senha = model.Senha;

        _context.SaveChanges();
      }
    }

    public void RemoverUsuario(Guid idUsuario)
    {
      var model = BuscarUsuario(idUsuario);

      if (model != null)
      {
        var pessoa = model.Pessoa;

        _context.Usuarios.Remove(model);
        _context.SaveChanges();
      }
    }

    public void AlterarSenhaUsuario(Guid idUsuario, string novaSenhaCriptografada)
    {
      var model = _context.Usuarios
        .FirstOrDefault(a => a.IDUsuario.Equals(idUsuario));

      if (model != null)
      {
        model.Senha = novaSenhaCriptografada;
        _context.SaveChanges();
      }
    }
  }
}