﻿using System;
using System.Linq;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class PessoaRepository : RepositoryBase<Pessoa>, IPessoaRepository
  {
    private readonly Context _context;

    public PessoaRepository(Context context) : base(context)
    {
      _context = context;
    }

    public void CadastrarPessoa(Pessoa model)
    {
      Add(model, true);
      Save();
    }

    public Pessoa BuscarPessoaUsuario(Guid idUsuario)
    {
      return _context.Usuarios
        .Where(a => a.IDUsuario.Equals(idUsuario))
        .Select(a => a.Pessoa)
        .FirstOrDefault();
    }

    public void EditarPessoa(Pessoa model)
    {
      Edit(model, true);
      _context.SaveChanges();
    }
  }
}