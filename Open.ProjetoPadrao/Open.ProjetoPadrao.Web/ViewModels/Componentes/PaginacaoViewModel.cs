﻿//using PagedList;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class PaginacaoViewModel
  {
    //public IPagedList<object> ListaPaginacao { get; set; }
    public string Action { get; set; }
    public string Status { get; set; }
    public object Filtro { get; set; }
  }
}