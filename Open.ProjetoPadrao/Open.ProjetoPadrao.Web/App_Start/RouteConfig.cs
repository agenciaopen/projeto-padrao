﻿using System.Web.Mvc;
using System.Web.Routing;
using Open.ProjetoPadrao.Web.Constraints;

namespace Open.ProjetoPadrao.Web
{
  public class RouteConfig
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
      routes.LowercaseUrls = true;

      routes.MapRoute("empreendimentos-pt", "{idioma}/empreendimentos", new { controller = "Empreendimento", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("empreendimentos-en", "{idioma}/apartments-for-sale", new { controller = "Empreendimento", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });
      routes.MapRoute("empreendimentos-status-do-imovel-pt", "empreendimentos/{status}", new { controller = "Empreendimento", action = "BuscarImoveisPorStatus", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("empreendimentos-status-do-imovel-en", "apartments/{status}", new { controller = "Empreendimento", action = "BuscarImoveisPorStatus", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });
      routes.MapRoute("empreendimentos-detalhe-pt", "empreendimentos/{nomeCidade}/{nomeEmpreendimento}", new { controller = "Empreendimento", action = "Detalhe", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("empreendimentos-detalhe-en", "apartments/{nomeCidade}/{nomeEmpreendimento}", new { controller = "Empreendimento", action = "Detalhe", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });
      
      routes.MapRoute("acompanhe-sua-obra-pt", "{idioma}/acompanhe-sua-obra", new { controller = "AcompanheSuaObra", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("acompanhe-sua-obra-en", "{idioma}/check-construction-progress", new { controller = "AcompanheSuaObra", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("venda-seu-terreno-pt", "{idioma}/venda-seu-terreno", new { controller = "VendaSeuTerreno", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("venda-seu-terreno-en", "{idioma}/sell-your-terrain", new { controller = "VendaSeuTerreno", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("institucional-pt", "{idioma}/institucional", new { controller = "Institucional", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("institucional-en", "{idioma}/institutional", new { controller = "Institucional", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("portal-do-cliente-pt", "{idioma}/portal-do-cliente", new { controller = "PortalCliente", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("portal-do-cliente-en", "{idioma}/client-portal", new { controller = "PortalCliente", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("fale-conosco-pt", "{idioma}/institucional/fale-conosco", new { controller = "Institucional", action = "FaleConosco", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("fale-conosco-en", "{idioma}/institutional/contact-us", new { controller = "Institucional", action = "FaleConosco", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("grupo-ayoshii-pt", "{idioma}/institucional/grupo-ayoshii", new { controller = "Institucional", action = "GrupoAyoshii", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("grupo-ayoshii-en", "{idioma}/institutional/ayoshii-group", new { controller = "Institucional", action = "GrupoAyoshii", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("historia-pt", "{idioma}/institucional/historia", new { controller = "Institucional", action = "Historia", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("historia-en", "{idioma}/institutional/history", new { controller = "Institucional", action = "Historia", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("missao-valores-pt", "{idioma}/institucional/missao-visao-valores", new { controller = "Institucional", action = "MissaoVisaoValores", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("missao-valores-en", "{idioma}/institutional/mission-vision-values", new { controller = "Institucional", action = "MissaoVisaoValores", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("premios-certificacoes-pt", "{idioma}/institucional/premios-e-certificacoes", new { controller = "Institucional", action = "PremiosCertificacoes", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("premios-certificacoes-en", "{idioma}/institutional/prizes-and-certifications", new { controller = "Institucional", action = "PremiosCertificacoes", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("atsushi-instituto-pt", "{idioma}/institucional/instituto-atsushi-kimiko-yoshii", new { controller = "Institucional", action = "InstitutoAtsushi", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("atsushi-instituto-en", "{idioma}/institutional/atsushi-kimiko-yoshii-institute", new { controller = "Institucional", action = "InstitutoAtsushi", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("sala-de-imprensa-pt", "{idioma}/institucional/sala-de-imprensa", new { controller = "Institucional", action = "SalaDeImprensa", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("sala-de-imprensa-en", "{idioma}/institutional/press-room", new { controller = "Institucional", action = "SalaDeImprensa", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("noticias-pt", "{idioma}/noticias", new { controller = "Noticias", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("noticias-en", "{idioma}/news", new { controller = "Noticias", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("noticia-detalhe-pt", "{idioma}/noticias/detalhe/{data}/{nomeNoticia}", new { controller = "Noticias", action = "Detalhe", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("noticia-detalhe-en", "{idioma}/news/detail/{data}/{nomeNoticia}", new { controller = "Noticias", action = "Detalhe", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("eventos-pt", "{idioma}/eventos", new { controller = "Eventos", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("eventos-en", "{idioma}/events", new { controller = "Eventos", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("evento-detalhe-pt", "{idioma}/eventos/detalhe/{data}/{nomeEvento}", new { controller = "Eventos", action = "Detalhe", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } } );
      routes.MapRoute("evento-detalhe-en", "{idioma}/events/detail/{data}/{nomeEvento}", new { controller = "Eventos", action = "Detalhe", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("trabalhe-conosco-pt", "{idioma}/institucional/trabalhe-conosco", new { controller = "Institucional", action = "TrabalheConosco", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("trabalhe-conosco-en", "{idioma}/institutional/work-with-us", new { controller = "Institucional", action = "TrabalheConosco", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });
      
      routes.MapRoute("depoimentos-pt", "{idioma}/depoimentos", new { controller = "Depoimentos", action = "Index", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("depoimentos-en", "{idioma}/testimonials", new { controller = "Depoimentos", action = "Index", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute("salvar-contato-pt", "{idioma}/contato/salvar-contato", new { controller = "Contato", action = "SalvarContato", idioma = "pt-br" }, new { idioma = new LanguageRouteConstraint { Idioma = "pt-br" } });
      routes.MapRoute("salvar-contato-en", "{idioma}/contact/save-contact", new { controller = "Contato", action = "SalvarContato", idioma = "en-us" }, new { idioma = new LanguageRouteConstraint { Idioma = "en-us" } });

      routes.MapRoute(
        name: "DefaultLocalized",
        url: "{idioma}/{controller}/{action}/{id}",
        constraints: new { idioma = "pt-BR|en-US" },
        defaults: new
        {
          controller = "Home",
          action = "Index",
          id = UrlParameter.Optional,
          idioma = "pt-br"
        });

      routes.MapRoute(
        name: "Default",
        url: "{controller}/{action}/{id}",
        defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
    }
  }
}