﻿using Open.ProjetoPadrao.Web.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class PaginaViewModel
  {
    [Key]
    public Guid IDPagina { get; set; }

    [Required(ErrorMessage = "Informe o nome da página")]
    [MaxLength(255)]
    public string Nome { get; set; }

    [Required(ErrorMessage = "Informe o identificador da página")]
    [MaxLength(50)]
    public string Identificador { get; set; }

    public IEnumerable<PaginaControle> PaginaControles { get; set; }

    [NotMapped]
    public IEnumerable<Controle> ListaControles { get; set; }
  }
}