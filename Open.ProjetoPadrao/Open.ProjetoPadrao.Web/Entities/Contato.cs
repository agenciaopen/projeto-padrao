﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Contato")]
  public class Contato
  {
    public Contato()
    {
      ListaContatoAnexo = new Collection<ContatoAnexo>();
    }

    [Key]
    public Guid IDContato { get; set; }

    [MaxLength(255)]
    public string Nome { get; set; }

    [Required, MaxLength(255)]
    public string Email { get; set; }

    [MaxLength(20)]
    public string Telefone { get; set; }

    [MaxLength(255)]
    public string Assunto { get; set; }

    [MaxLength(500)]
    public string Mensagem { get; set; }

    [MaxLength(255)]
    public string Departamento { get; set; }

    [Required]
    public int CodigoTipoContato
    {
      get
      {
        return (int)TipoContato;
      }
      set
      {
        TipoContato = (TipoContato)value;
      }
    }

    [EnumDataType(typeof(TipoContato)), NotMapped]
    public TipoContato TipoContato { get; set; }

    public bool Visualizado { get; set; }

    [Required]
    public DateTime DataHoraInclusao { get; set; }

    [MaxLength(20)]
    public string Celular { get; set; }

    [MaxLength(255)]
    public string NomeEmpresa { get; set; }

    [MaxLength(255)]
    public string RelacaoTerreno { get; set; }

    [MaxLength(255)]
    public string Parceria { get; set; }

    [MaxLength(500)]
    public string Proposta { get; set; }

    [MaxLength(255)]
    public string NomeEmpreendimento { get; set; }

    public Guid? IDCidade { get; set; }

    public Guid? IDTerreno { get; set; }

    public bool? MoraExterior { get; set; }

    public virtual ICollection<ContatoAnexo> ListaContatoAnexo { get; set; }

    public virtual Cidade Cidade { get; set; }

    [MaxLength(255)]
    public virtual string NumeroContribuinte { get; set; }
  }
}