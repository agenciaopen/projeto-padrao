﻿using System;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IEnderecoRepository : IRepositoryBase<Endereco>
  {
    Endereco BuscarEndereco(Guid idEndereco);
    void SalvarEndereco(Endereco model);
  }
}