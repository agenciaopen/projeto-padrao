﻿using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.UnitOfWork;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class PrincipalController : BaseController
  {
    public PrincipalController(IUnitOfWork unitOfWork) : base(unitOfWork) { }

    [HttpGet]
    public ActionResult Index()
    {
      return View();
    }

    [HttpGet, ChildActionOnly]
    public ActionResult Menu()
    {
      if (UsuarioLogado.MenusPermitidos == null)
      {
        if (Session["MenusPermitidos"] == null)
          Session["MenusPermitidos"] = ConstruirMenus();
        
        UsuarioLogado.MenusPermitidos = Session["MenusPermitidos"] as List<Menu>;
      }

      return PartialView("_PartialMenu");
    }

    private List<Menu> ConstruirMenus()
    {
      List<Menu> menusConstruidos = new List<Menu>();

      List<Menu> menusPermitidosPerfil = _unitOfWork
        .UsuarioPerfilMenuRepository
        .BuscarMenusPerfil(UsuarioLogado.IDUsuarioPerfil)
        .ToList();

      foreach (var itemPermitido in menusPermitidosPerfil)
      {
        Menu itemMenuConstruido = new Menu()
        {
          IDMenu = itemPermitido.IDMenu,
          Controller = itemPermitido.Controller,
          Action = itemPermitido.Action,
          OrdemExibicao = itemPermitido.OrdemExibicao,
          TextoExibicao = itemPermitido.TextoExibicao,
          Icone = itemPermitido.Icone,
        };

        List<Menu> menusFilhos = menusPermitidosPerfil
          .Where(m => m.IDMenuPai.Equals(itemPermitido.IDMenu))
          .ToList();

        foreach (var menuFilho in menusFilhos)
        {
          List<Menu> subMenusFilhos = menusPermitidosPerfil
            .Where(m => m.IDMenuPai.Equals(menuFilho.IDMenu)
            ).ToList();

          foreach (Menu subMenuFilho in subMenusFilhos)
            menuFilho.MenusFilhos.Add(subMenuFilho);

          itemMenuConstruido.MenusFilhos.Add(menuFilho);
        }

        if (itemPermitido.IDMenuPai == null)
          menusConstruidos.Add(itemMenuConstruido);
      }

      return menusConstruidos;
    }
  }
}