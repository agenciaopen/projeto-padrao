﻿using Open.ProjetoPadrao.Web.Enums;
using System;
using System.Web.Mvc;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public class ContatoViewModel
  {
    public ContatoViewModel()
    {
      IDContato = Guid.NewGuid();
    }

    public Guid IDContato { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
    public string Celular { get; set; }
    public string Mensagem { get; set; }
    public string NomeEmpresa { get; set; }
    public string NomeEmpreendimento { get; set; }
    public int RelacaoTerreno { get; set; }
    public string Proposta { get; set; }
    public bool CadastrarNewsletter { get; set; }
    public Guid IDEstado { get; set; }
    public Guid IDCidade { get; set; }
    public Guid IDEmpreendimento { get; set; }
    public bool MoraExterior { get; set; }
    public TipoContato TipoContato { get; set; }
    public string Controller { get; set; }
    public string NumeroContribuinte { get; set; }

    public SelectList Estados { get; set; }
  }
}