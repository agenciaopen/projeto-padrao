﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Endereco")]
  public class Endereco
  {
    [Key, Required]
    public Guid IDEndereco { get; set; }

    [Required]
    public Guid IDBairro { get; set; }

    [Required, MaxLength(50)]
    public string TipoLogradouro { get; set; }

    [Required, MaxLength(500)]
    public string Logradouro { get; set; }

    public int? Numero { get; set; }

    [MaxLength(255)]
    public string Complemento { get; set; }

    [MaxLength(10)]
    public string CEP { get; set; }
    
    public virtual Bairro Bairro { get; set; }
  }
}