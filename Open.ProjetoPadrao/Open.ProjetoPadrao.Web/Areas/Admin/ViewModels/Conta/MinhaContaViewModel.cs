﻿using System.ComponentModel.DataAnnotations;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class MinhaContaViewModel
  {
    [Required(ErrorMessage = "Informe a senha atual")]
    [StringLength(32, ErrorMessage = "A senha atual deve ter no mínimo 1 e no máximo 32 caracteres", MinimumLength = 1)]
    [DataType(DataType.Password)]
    [Display(Name = "Senha Atual")]
    public string SenhaAtual { get; set; }

    [Required(ErrorMessage = "Informe a nova senha")]
    [StringLength(32, ErrorMessage = "A nova senha deve ter no mínimo 6 e no máximo 32 caracteres", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Nova Senha")]
    public string NovaSenha { get; set; }

    [Required(ErrorMessage = "Confirme a senha")]
    [StringLength(32, ErrorMessage = "A nova senha deve ter no mínimo 6 e no máximo 32 caracteres", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Compare("NovaSenha", ErrorMessage = "As senhas não coincidem")]
    [Display(Name = "Confirme a Senha")]
    public string ConfirmacaoSenha { get; set; }
  }
}