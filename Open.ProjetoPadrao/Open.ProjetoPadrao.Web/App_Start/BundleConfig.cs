﻿using System.Web.Optimization;

namespace Open.ProjetoPadrao.Web
{
  public class BundleConfig
  {
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new StyleBundle("~/bundles/admin/css/login")
        .Include("~/Areas/Admin/Assets/fonts/font-awesome/css/fontawesome-all.css", new CssRewriteUrlTransform())
        .Include("~/Areas/Admin/Assets/css/bootstrap.css",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.min.css",
                 "~/Areas/Admin/Assets/css/login.css"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/login")
        .Include("~/Areas/Admin/Assets/js/jquery-3.3.1.js",
                 "~/Areas/Admin/Assets/js/jquery.validate.js",
                 "~/Areas/Admin/Assets/js/jquery.validate.unobtrusive.js",
                 "~/Areas/Admin/Assets/js/jquery.unobtrusive-ajax.js",
                 "~/Areas/Admin/Assets/js/bootstrap.js",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.jquery.min.js",
                 "~/Areas/Admin/Assets/plugins/ladda/spin.min.js",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.min.js",
                 "~/Areas/Admin/Assets/js/app/login.js"));

      bundles.Add(new StyleBundle("~/bundles/admin/css/geral")
        .Include("~/Areas/Admin/Assets/fonts/font-awesome/css/fontawesome-all.css", new CssRewriteUrlTransform())
        .Include("~/Areas/Admin/Assets/css/bootstrap.css",
                 "~/Areas/Admin/Assets/css/animate.css",
                 "~/Areas/Admin/Assets/css/style.css",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.min.css",
                 "~/Areas/Admin/Assets/plugins/toastr/toastr.css",
                 "~/Areas/Admin/Assets/plugins/datatables/datatables.css",
                 "~/Areas/Admin/Assets/plugins/datatables/dataTables.rowReorder.min.css")
        .Include("~/Areas/Admin/Assets/css/personalizado.css", new CssRewriteUrlTransform()));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/geral")
        .Include("~/Areas/Admin/Assets/js/jquery-3.3.1.js",
                 "~/Areas/Admin/Assets/plugins/globalize/globalize.js",
                 "~/Areas/Admin/Assets/plugins/globalize/globalize.culture.pt-BR.js",
                 "~/Areas/Admin/Assets/js/jquery.validate.js",
                 "~/Areas/Admin/Assets/js/jquery.validate.unobtrusive.js",
                 "~/Areas/Admin/Assets/js/jquery.unobtrusive-ajax.js",
                 "~/Areas/Admin/Assets/js/bootstrap.js",
                 "~/Areas/Admin/Assets/plugins/metisMenu/metisMenu.js",
                 "~/Areas/Admin/Assets/plugins/pace/pace.js",
                 "~/Areas/Admin/Assets/plugins/slimscroll/jquery.slimscroll.js",
                 "~/Areas/Admin/Assets/plugins/popper/popper.js",
                 "~/Areas/Admin/Assets/plugins/sweetmodal/jquery.sweet-modal.js",
                 "~/Areas/Admin/Assets/plugins/sweetalert/sweetalert.min.js",
                 "~/Areas/Admin/Assets/plugins/toastr/toastr.js",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.jquery.min.js",
                 "~/Areas/Admin/Assets/plugins/ladda/spin.min.js",
                 "~/Areas/Admin/Assets/plugins/ladda/ladda.min.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/inputmask.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/inputmask.extensions.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/inputmask.numeric.extensions.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/inputmask.date.extensions.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/inputmask.phone.extensions.js",
                 "~/Areas/Admin/Assets/plugins/inputMask/jquery.inputmask.js",
                 "~/Areas/Admin/Assets/plugins/datepicker/bootstrap-datepicker.js",
                 "~/Areas/Admin/Assets/plugins/datepicker/bootstrap-datepicker.pt-BR.min.js",
                 "~/Areas/Admin/Assets/js/app/app.js"));

      // PLUGIN - SUMMERNOTE
      bundles.Add(new StyleBundle("~/bundles/admin/css/plugins/summernote")
        .Include("~/Areas/Admin/Assets/plugins/summernote/summernote.css", new CssRewriteUrlTransform()));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/plugins/summernote")
        .Include("~/Areas/Admin/Assets/plugins/summernote/summernote.js",
                 "~/Areas/Admin/Assets/plugins/summernote/lang/summernote-pt-BR.js",
                 "~/Areas/Admin/Assets/plugins/summernote/initial.js"));

      // PLUGIN - DROPZONE
      bundles.Add(new ScriptBundle("~/bundles/admin/js/plugins/dropzone")
        .Include("~/Areas/Admin/Assets/plugins/dropzone/dropzone.js"));

      bundles.Add(new StyleBundle("~/bundles/admin/css/plugins/dropzone")
       .Include("~/Areas/Admin/Assets/plugins/dropzone/dropzone.css"));

      // PLUGIN - DATATABLES
      bundles.Add(new ScriptBundle("~/bundles/admin/js/plugins/datatables")
        .Include("~/Areas/Admin/Assets/plugins/datatables/datatables.js",
                 "~/Areas/Admin/Assets/plugins/datatables/dataTables.rowReorder.min.js"));

      bundles.Add(new StyleBundle("~/bundles/admin/css/plugins/datatables")
        .Include("~/Areas/Admin/Assets/plugins/datatables/datatables.css"));

      // PLUGIN - LIGHTBOX
      bundles.Add(new StyleBundle("~/bundles/admin/css/plugins/lightbox")
      .Include("~/Areas/Admin/Assets/plugins/lightbox/css/lightbox.css", new CssRewriteUrlTransform()));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/plugins/lightbox")
        .Include("~/Areas/Admin/Assets/plugins/lightbox/js/lightbox.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/plugins/rangeslider")
       .Include("~/Areas/Admin/Assets/plugins/rangeslider/rangeslider.js"));

      bundles.Add(new StyleBundle("~/bundles/admin/css/plugins/rangeslider")
       .Include("~/Areas/Admin/Assets/plugins/rangeslider/rangeslider.css", new CssRewriteUrlTransform()));



      // PÁGINAS

      bundles.Add(new ScriptBundle("~/bundles/admin/js/pagina")
        .Include("~/Areas/Admin/Assets/js/app/pagina.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/empreendimento")
        .Include("~/Areas/Admin/Assets/js/app/empreendimento.js",
                 "~/Areas/Admin/Assets/plugins/radialindicator/radialIndicator.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/bairro")
        .Include("~/Areas/Admin/Assets/js/app/bairro.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/controle")
        .Include("~/Areas/Admin/Assets/js/app/controle.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/contato")
        .Include("~/Areas/Admin/Assets/js/app/contato.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/video")
        .Include("~/Areas/Admin/Assets/js/app/video.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/linhatempo")
        .Include("~/Areas/Admin/Assets/js/app/linhatempo.js"));
      
      bundles.Add(new ScriptBundle("~/bundles/admin/js/pontovenda")
        .Include("~/Areas/Admin/Assets/js/app/pontovenda.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/noticia")
        .Include("~/Areas/Admin/Assets/js/app/noticia.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/evento")
        .Include("~/Areas/Admin/Assets/js/app/evento.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/depoimento")
        .Include("~/Areas/Admin/Assets/js/app/depoimento.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/newsletter")
        .Include("~/Areas/Admin/Assets/js/app/newsletter.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/perfil")
        .Include("~/Areas/Admin/Assets/js/app/perfil.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/usuario")
        .Include("~/Areas/Admin/Assets/js/app/usuario.js"));
      
      bundles.Add(new ScriptBundle("~/bundles/admin/js/servicocolaborador")
        .Include("~/Areas/Admin/Assets/js/app/servicocolaborador.js"));

      bundles.Add(new ScriptBundle("~/bundles/admin/js/ayoshiinews")
        .Include("~/Areas/Admin/Assets/js/app/ayoshiinews.js"));

      // SITE

      bundles.Add(new ScriptBundle("~/bundles/site/js/geral")
        .Include("~/Content/js/geral.js"));
      
      bundles.Add(new ScriptBundle("~/bundles/js/simulacao")
        .Include("~/Scripts/Imovel/_simulacao.js"));

      bundles.Add(new ScriptBundle("~/bundles/js/imovel/detalhe")
        .Include("~/Scripts/classeGoogleMaps.js")
        .Include("~/Scripts/Imovel/detalhe.js"));
      
      #if DEBUG
        BundleTable.EnableOptimizations = false;
      #else
        BundleTable.EnableOptimizations = false;
      #endif
    }
  }
}