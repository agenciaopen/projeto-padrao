﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Authentication;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.Framework.Uteis;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Properties;
using AutoMapper;
using Open.ProjetoPadrao.Web.ViewModels.Admin;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class BaseController : Controller
  {
    protected readonly IUnitOfWork _unitOfWork;
    protected Mail ConfiguracaoEmail = new Mail(Configs.MailSender, Configs.MailPassword, Configs.MailAlias, Configs.MailSMTP, short.Parse(Configs.MailPort), bool.Parse(Configs.MailSSL));

    public BaseController(IUnitOfWork unitOfWork)
    {
      _unitOfWork = unitOfWork;
    }

    public virtual UsuarioPrincipal UsuarioLogado
    {
      get { return User as UsuarioPrincipal; }
    }

    internal void ValidarForgeryTokenAjax(string tokenHeader)
    {
      var antiForgeryCookie = HttpContext.Request.Cookies[AntiForgeryConfig.CookieName];
      var cookieToken = antiForgeryCookie != null ? antiForgeryCookie.Value : null;

      AntiForgery.Validate(cookieToken, tokenHeader);
    }

    [HttpPost]
    public JsonResult BuscarCidades(Guid idEstado)
    {
      return Json(_unitOfWork.CidadeRepository
        .BuscarCidades(idEstado)
        .Select(a => new SelectListItem()
        {
          Value = a.IDCidade.ToString(),
          Text = a.Nome
        }));
    }

    [HttpPost]
    public JsonResult BuscarBairros(Guid idCidade)
    {
      return Json(_unitOfWork.BairroRepository
        .BuscarBairros(idCidade)
        .Select(a => new SelectListItem()
        {
          Value = a.IDBairro.ToString(),
          Text = a.Nome
        }));
    }

    [HttpPost]
    public JsonResult BuscarEnderecoCEP(string cep)
    {
      return Json(new Correios().BuscarEnderecoPorCEP(cep));
    }

    protected SelectList PreencherListaSimNao(bool? valorSelecionado = null)
    {
      var lista = new List<SelectListItem>();

      lista.Add(new SelectListItem() { Value = "true", Text = "Sim" });
      lista.Add(new SelectListItem() { Value = "false", Text = "Não" });

      return new SelectList(lista, "Value", "Text", valorSelecionado);
    }

    protected SelectList PreencherListaTiposLogradouros()
    {
      var lista = new List<SelectListItem>();

      lista.Add(new SelectListItem() { Value = "Rua", Text = "Rua" });
      lista.Add(new SelectListItem() { Value = "Avenida", Text = "Avenida" });
      lista.Add(new SelectListItem() { Value = "Alameda", Text = "Alameda" });
      lista.Add(new SelectListItem() { Value = "Praça", Text = "Praça" });
      lista.Add(new SelectListItem() { Value = "Jardim", Text = "Jardim" });

      return new SelectList(lista.OrderBy(a => a.Text), "Value", "Text");
    }

    internal bool DiretorioVazio(string caminho)
    {
      IEnumerable<string> items = Directory.EnumerateFileSystemEntries(caminho);
      using (IEnumerator<string> en = items.GetEnumerator())
      {
        return !en.MoveNext();
      }
    }

    #region WebSite

    internal IEnumerable<PaginaControleParametrosViewModel> BuscarParametrosPagina(string identificadorPagina)
    {
      IEnumerable<PaginaControleParametro> model = _unitOfWork.PaginaControleParametroRepository
                                                              .BuscarParametrosPagina(identificadorPagina);

      return Mapper.Map<IEnumerable<PaginaControleParametro>, IEnumerable<PaginaControleParametrosViewModel>>(model);
    }

    [HttpPost]
    public JsonResult ListarCidades(Guid idCidade)
    {
      return Json(_unitOfWork.CidadeRepository.BuscarCidades(idCidade).Select(s => new { s.Nome, s.IDCidade }));
    }

    #region Arquivos

    protected Arquivo ExtrairArquivo(HttpPostedFileBase arquivo)
    {
      string novoNomeArquivo = Guid.NewGuid().ToString().Replace("-", "");
      return new Arquivo
      {
        IDArquivo = Guid.NewGuid(),
        NomeReal = novoNomeArquivo + Path.GetExtension(arquivo.FileName),
        NomeHash = novoNomeArquivo,
        Extensao = Path.GetExtension(arquivo.FileName),
        MimeType = arquivo.ContentType.ToLower(),
        TamanhoKb = arquivo.ContentLength / 1024,
        ServidorHospedagem = "Local",
        Caminho = "Uploads/"
      };
    }

    protected void UploadArquivo(HttpPostedFileBase arquivoAnexo, Arquivo arquivo)
    {
      if (arquivoAnexo != null && arquivoAnexo.ContentLength > 0)
      {
        string path = Path.Combine(Server.MapPath("~/" + arquivo.Caminho), arquivo.NomeReal);
        arquivoAnexo.SaveAs(path);
      }
    }

    #endregion

    #endregion
  }
}