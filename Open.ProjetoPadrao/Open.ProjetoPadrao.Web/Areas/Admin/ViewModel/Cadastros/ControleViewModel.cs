﻿using System;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class ControleViewModel
  {
    public Guid IDControle { get; set; }

    public string Nome { get; set; }
  
    public string Icone { get; set; }

    public string Descricao { get; set; }
  }
}