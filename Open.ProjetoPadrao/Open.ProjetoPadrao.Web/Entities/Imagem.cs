﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Imagem")]
  public class Imagem
  {
    [Key]
    public Guid IDImagem { get; set; }

    [Required]
    public Guid IDArquivo { get; set; }

    [Required]
    [MaxLength(255)]
    public string Alt { get; set; }

    [Required]
    [MaxLength(255)]
    public string Tooltip { get; set; }
  }
}