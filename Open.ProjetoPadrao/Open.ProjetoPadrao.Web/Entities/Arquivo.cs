﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Arquivo")]
  public class Arquivo
  {
    public Arquivo()
    {
      ContatoAnexos = new List<ContatoAnexo>();
    }

    [Key]
    public Guid IDArquivo { get; set; }

    [Required, MaxLength(255)]
    public string NomeReal { get; set; }

    [Required, MaxLength(255)]
    public string NomeHash { get; set; }

    [Required]
    [MaxLength(255)]
    public string Extensao { get; set; }

    [Required]
    [MaxLength(255)]
    public string MimeType { get; set; }

    [Required]
    public int TamanhoKb{ get; set; }

    [Required]
    [MaxLength(50)]
    public string ServidorHospedagem { get; set; }

    [Required]
    public string Caminho { get; set; }

    [NotMapped]
    public Stream Stream { get; set; }

    public virtual ICollection<ContatoAnexo> ContatoAnexos { get; set; }

    public static implicit operator Arquivo(Framework.Classes.Arquivo arquivoFramework)
    {
      return new Arquivo()
      {
        Caminho = arquivoFramework.Caminho,
        Extensao = arquivoFramework.Extensao,
        IDArquivo = arquivoFramework.IDArquivo,
        MimeType = arquivoFramework.MimeType,
        NomeHash = arquivoFramework.NomeHash,
        NomeReal = arquivoFramework.NomeReal,
        ServidorHospedagem = arquivoFramework.ServidorHospedagem,
        Stream = arquivoFramework.Stream,
        TamanhoKb = arquivoFramework.TamanhoKb
      };
    }
  }
}