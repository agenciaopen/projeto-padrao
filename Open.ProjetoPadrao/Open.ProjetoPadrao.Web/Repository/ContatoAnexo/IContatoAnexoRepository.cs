﻿using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IContatoAnexoRepository : IRepositoryBase<ContatoAnexo>
  {
  }
}