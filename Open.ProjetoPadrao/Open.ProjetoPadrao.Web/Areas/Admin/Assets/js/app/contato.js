﻿$(document).on("click", "[data-id='btnRemoverContato']", function () {
  var $this = $(this);
  var url = $this.data("url");
  var idContato = $this.data("idcontato");

  swal({
    text: "Deseja realmente deletar este contato?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {
        $this.button('loading');
        $.ajax({
          url: url,
          type: "POST",
          data: { idContato: idContato },
          success: function (contatos) {
            $this.button('reset');
            $("#divGridContatos").html(contatos);
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirAlerta("E", "Falha ao remover o contato");
          }
        });
      }
    });
});