﻿using System.Web;
using System.Web.Mvc;

namespace Open.ProjetoPadrao.Web.Attributes
{
  public class CustomAuthorizeAttribute : AuthorizeAttribute
  {
    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
      var routeData = httpContext.Request.RequestContext.RouteData;
      var controller = routeData.GetRequiredString("controller");
      var action = routeData.GetRequiredString("action");
      var area = routeData.DataTokens["area"];
      var user = httpContext.User;

      if (area != null && area.ToString().Equals("Admin"))
      {
        if (!user.Identity.IsAuthenticated)
          return false;
      }
      return true;
    }
  }
}