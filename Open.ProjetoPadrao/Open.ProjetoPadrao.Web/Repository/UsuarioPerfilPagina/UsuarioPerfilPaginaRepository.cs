﻿using System.Data;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class UsuarioPerfilPaginaRepository : RepositoryBase<UsuarioPerfilPagina>, IUsuarioPerfilPaginaRepository
  {
    private readonly Context _context;

    public UsuarioPerfilPaginaRepository(Context context) : base(context)
    {
      _context = context;
    }

    public List<Pagina> BuscarPaginasPerfil()
    {
      return _context.UsuarioPerfilPaginas
        .Select(b => b.Pagina)
        .ToList();
    }
  }
}