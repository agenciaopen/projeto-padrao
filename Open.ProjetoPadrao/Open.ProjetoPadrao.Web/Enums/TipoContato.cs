﻿namespace Open.ProjetoPadrao.Web.Enums
{
  public enum TipoContato
  {
    FaleConosco = 1,
    VendaSeuTerreno = 2,
    Parceria = 3,
    Contato = 5,
    LigamosParaVoce = 6
  }
}