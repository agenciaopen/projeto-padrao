﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IPaginaControleParametroRepository : IRepositoryBase<PaginaControleParametro>
  {
    void RemoverTodosParametrosControle(Guid idPaginaControle);
    void AdicionarParametrosControle(List<PaginaControleParametro> model);
    IEnumerable<PaginaControleParametro> BuscarParametrosPagina(string identificadorPagina);
  }
}