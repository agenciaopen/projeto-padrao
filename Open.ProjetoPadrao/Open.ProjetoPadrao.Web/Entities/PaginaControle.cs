﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("PaginaControle")]
  public class PaginaControle
  {
    public PaginaControle()
    {
      this.PaginaControleParametros = new Collection<PaginaControleParametro>();
      this.PaginaControleImagens = new Collection<PaginaControleImagem>();
    }

    [Key]
    public Guid IDPaginaControle { get; set; }

    [Required]
    public Guid IDPagina { get; set; }

    [Required]
    public Guid IDControle { get; set; }

    public string NomeIdentificadorControle { get; set; }

    public string Descricao { get; set; }

    public bool Configurado { get; set; }

    public virtual Controle Controle { get; set; }

    public virtual Pagina Pagina { get; set; }

    public virtual ICollection<PaginaControleParametro> PaginaControleParametros { get; set; }

    public virtual ICollection<PaginaControleImagem> PaginaControleImagens { get; set; }
  }
}