﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class VitrineViewModel
  {
    public string CaminhoImagem { get; set; }
    public string Alt { get; set; }
    public string Title { get; set; }
    public string Legenda { get; set; }
    public string Titulo { get; set; }
    public string SubTitulo { get; set; }
    public string Url { get; set; }
    public DateTime Data { get; set; }
  }
}