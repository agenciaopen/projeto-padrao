﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Open.ProjetoPadrao.Web
{
  public static class LanguageHelper
  {
    public static MvcHtmlString LangSwitcher(this UrlHelper url, string Name, RouteData routeData, string idioma)
    {
      var liTagBuilder = new TagBuilder("li");
      var aTagBuilder = new TagBuilder("a");
      var iTagBuilder = new TagBuilder("i");

      var routeValueDictionary = new RouteValueDictionary(routeData.Values);

      if (routeValueDictionary.ContainsKey("idioma"))
        if (routeData.Values["idioma"] as string == idioma)
          liTagBuilder.AddCssClass("active");
        else
          routeValueDictionary["idioma"] = idioma;

      routeValueDictionary["controller"] = "home";
      routeValueDictionary["action"] = "index";
      
      aTagBuilder.MergeAttribute("href", url.RouteUrl(routeValueDictionary));
      aTagBuilder.MergeAttribute("data-id", "linkIdioma");
      aTagBuilder.MergeAttribute("data-idioma", idioma);

      iTagBuilder.AddCssClass("icon-" + (idioma == "pt-BR" ? "brasil" : "united-states"));
      aTagBuilder.InnerHtml = iTagBuilder.ToString();
      liTagBuilder.InnerHtml = aTagBuilder.ToString();
      
      return new MvcHtmlString(liTagBuilder.ToString());
    }
  }
}