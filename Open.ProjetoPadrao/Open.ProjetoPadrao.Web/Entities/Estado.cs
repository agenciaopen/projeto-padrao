﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Estado")]
  public class Estado
  {
    public Estado()
    {
      this.ListaCidades = new List<Cidade>();
    }

    [Key]
    public Guid IDEstado { get; set; }

    [Required]
    public string NomeCompleto { get; set; }

    [Required]
    public string Sigla { get; set; }

    public virtual ICollection<Cidade> ListaCidades { get; set; }
  }
}