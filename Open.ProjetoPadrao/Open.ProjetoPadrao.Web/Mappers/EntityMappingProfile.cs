﻿using System;
using System.Linq;
using AutoMapper;
using Open.Framework.Extensoes;
using Open.ProjetoPadrao.Web.Enums;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.ViewModels.Site;
using System.Globalization;
using System.Threading;
using Open.ProjetoPadrao.Web.Properties;
using System.Collections.Generic;

namespace Open.ProjetoPadrao.Web.Mappers
{
  public class EntityMappingProfile : Profile
  {
    public EntityMappingProfile()
    {
      CreateMap<PaginaControleImagem, VitrineViewModel>()
        .ForMember(a => a.CaminhoImagem, b => b.MapFrom(src => src.Arquivo != null ? "/" + src.Arquivo.Caminho.ObterTextoApartirPalavra("Uploads") + "/" + src.Arquivo.NomeHash + src.Arquivo.Extensao : null))
        .ForMember(a => a.Titulo, b => b.MapFrom(src => BuscarIdiomaAtual() == Idioma.pt ? src.TextoTitulo : src.TextoTituloEN))
        .ForMember(a => a.SubTitulo, b => b.MapFrom(src => BuscarIdiomaAtual() == Idioma.pt ? src.TextoSubTitulo : src.TextoSubTituloEN))
        .ForMember(a => a.Url, b => b.MapFrom(src => BuscarIdiomaAtual() == Idioma.pt ? src.URLRedirecionamento : src.URLRedirecionamentoEN))
        .ForMember(a => a.Alt, b => b.MapFrom(src => BuscarIdiomaAtual() == Idioma.pt ? src.Alt : src.AltEN))
        .ForMember(a => a.Legenda, b => b.MapFrom(src => BuscarIdiomaAtual() == Idioma.pt ? src.Legenda : src.LegendaEN))
        .ForMember(a => a.Data, b => b.MapFrom(src => src.DataHoraInclusao));
    }

    private Idioma BuscarIdiomaAtual()
    {
      CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;

      if (currentCulture.Name.Equals(Configs.Portugues))
        return Idioma.pt;
      else return Idioma.en;
    }
  }
}