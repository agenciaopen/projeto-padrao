﻿using System.Collections.Generic;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public sealed class PaginaGenericaViewModel
  {
    public IEnumerable<PaginaControleParametrosViewModel> PaginaControleParametros { get; set; }
  }
}