﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class UsuarioPerfisViewModel
  {
    [Key]
    public Guid IDUsuarioPerfil { get; set; }

    public string Nome { get; set; }
  
    public int QtdUsuarios { get; set; }

    public bool Administrador { get; set; }
  }
}