﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class VideosViewModel
  {
    [Key, Required]
    public Guid IDVideo { get; set; }

    public string Titulo { get; set; }

    public string Descricao { get; set; }

    public DateTime DataCriacao { get; set; }
  }
}