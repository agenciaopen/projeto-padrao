﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public sealed class PaginaControleParametrosViewModel
  {
    public Guid IDPaginaControleParametro { get; set; }

    public Guid IDPaginaControle { get; set; }

    public string IdentificadorControle { get; set; }

    public string ConteudoTexto { get; set; }

    public string ConteudoHeadline { get; set; }

    public string ConteudoHeadlineEN { get; set; }

    public string ConteudoTitleEN { get; set; }

    public string ConteudoDescriptionEN { get; set; }
  }
}