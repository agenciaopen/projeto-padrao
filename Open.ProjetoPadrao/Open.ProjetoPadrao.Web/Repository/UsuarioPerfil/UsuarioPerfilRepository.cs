﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class UsuarioPerfilRepository : RepositoryBase<UsuarioPerfil>, IUsuarioPerfilRepository
  {
    private readonly Context _context;

    public UsuarioPerfilRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<UsuarioPerfil> BuscarPerfis(bool adicionarSuperUser = false)
    {
      var model = _context.UsuarioPerfis
        .Include(a => a.Usuarios)
        .OrderBy(a => a.Nome);

      if (!adicionarSuperUser)
        model = model
          .Where(a => !a.SuperUser)
          .OrderBy(a => a.Nome);

      return model;
    }

    public UsuarioPerfil BuscarPerfil(Guid idUsuarioPerfil)
    {
      return _context.UsuarioPerfis
        .Include(a => a.UsuarioPerfilMenus)
        .FirstOrDefault(a => a.IDUsuarioPerfil.Equals(idUsuarioPerfil));
    }

    public void RemoverPerfil(Guid idUsuarioPerfil)
    {
      var model = BuscarPerfil(idUsuarioPerfil);

      if (model != null)
      {
        var menusPerfil = _context.UsuarioPerfilMenus
          .Where(a => a.IDUsuarioPerfil.Equals(idUsuarioPerfil));

        if (menusPerfil != null)
          _context.UsuarioPerfilMenus.RemoveRange(menusPerfil);

        Delete(model);
        Save();
      }
    }

    public void CadastrarPerfil(UsuarioPerfil model)
    {
      Add(model, true);
    }

    public void EditarUsuarioPerfil(UsuarioPerfil model)
    {
      Edit(model, true);
      _context.SaveChanges();
    }
  }
}