﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Newsletter")]
  public class Newsletter
  {
    [Key]
    public Guid IDNewsletter { get; set; }

    [Required, MaxLength(150)]
    public string Nome { get; set; }

    [Required, MaxLength(150)]
    public string Email { get; set; }

    [MaxLength(20)]
    public string Telefone { get; set; }

    [Required]
    public bool ReceberWhatsApp { get; set; }

    [Required]
    public bool ReceberTelefone { get; set; }
  
    [Required]
    public DateTime DataCadastro { get; set; }
  }
}