﻿namespace Open.ProjetoPadrao.Web.Enums
{
  public enum Idioma
  {
    pt = 1,
    en = 2
  }
}