﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("UsuarioPerfilMenu")]
  public class UsuarioPerfilMenu
  {
    [Key]
    public Guid IDUsuarioPerfilMenu { get; set; }

    [Required]
    public Guid IDUsuarioPerfil { get; set; }

    [Required]
    public Guid IDMenu { get; set; }

    public virtual Menu Menu { get; set; }

    public virtual UsuarioPerfil UsuarioPerfil { get; set; }
  }
}