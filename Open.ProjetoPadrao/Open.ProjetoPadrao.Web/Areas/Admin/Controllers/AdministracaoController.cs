﻿using AutoMapper;
using Open.Framework.Classes;
using Open.Framework.Seguranca;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.ViewModels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class AdministracaoController : BaseController
  {
    public AdministracaoController(IUnitOfWork unitOfWork) : base(unitOfWork) { }

    #region Perfis

    [HttpGet]
    public ActionResult Perfis()
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return RedirectToAction("Index", "Principal");

      var model = _unitOfWork
        .UsuarioPerfilRepository
        .BuscarPerfis(UsuarioLogado.SuperUser);

      var perfisViewModel = Mapper
        .Map<IEnumerable<UsuarioPerfil>, IEnumerable<UsuarioPerfisViewModel>>(model);

      return View(perfisViewModel);
    }

    [HttpGet]
    public ActionResult NovoPerfil()
    {
      if (!UsuarioLogado.SuperUser || !UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return RedirectToAction("Index", "Principal");

      var viewModel = new UsuarioPerfilViewModel();
      viewModel.ListaMenus = ConstruirMenus();

      return View(viewModel);
    }

    [HttpGet]
    public ActionResult Perfil(Guid id)
    {
      if (!UsuarioLogado.SuperUser || !UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return RedirectToAction("Index", "Principal");

      var model = _unitOfWork.UsuarioPerfilRepository.BuscarPerfil(id);
      var viewModel = Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(model);
      viewModel.ListaMenus = ConstruirMenus();

      return View(viewModel);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult CadastrarPerfil(UsuarioPerfilViewModel viewModel)
    {
      if (!UsuarioLogado.SuperUser || !UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      viewModel.IDUsuarioPerfil = Guid.NewGuid();

      var retorno = new RetornoJson(false, "Perfil cadastrado com sucesso", Url.Action("Perfis", "Administracao"));

      try
      {
        var model = Mapper.Map<UsuarioPerfilViewModel, UsuarioPerfil>(viewModel);
        model.UsuarioPerfilMenus = BuscarMenusString(viewModel.ListaMenusString, viewModel.IDUsuarioPerfil);
        model.DataHoraCriacao = DateTime.Now;

        _unitOfWork.UsuarioPerfilRepository.CadastrarPerfil(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.UrlRetorno = string.Empty;
        retorno.Mensagem = "Ocorreu um erro ao cadastrar o perfil";
      }

      return Json(retorno);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult EditarPerfil(UsuarioPerfilViewModel viewModel)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      var retorno = new RetornoJson(false, "Perfil editado com sucesso", string.Empty);

      try
      {
        var model = Mapper.Map<UsuarioPerfilViewModel, UsuarioPerfil>(viewModel);
        model.UsuarioPerfilMenus = BuscarMenusString(viewModel.ListaMenusString, viewModel.IDUsuarioPerfil);
        model.DataHoraAlteracao = DateTime.Now;

        _unitOfWork.UsuarioPerfilMenuRepository.RemoverMenusPerfil(viewModel.IDUsuarioPerfil);
        _unitOfWork.UsuarioPerfilMenuRepository.SalvarMenusPerfil(model.UsuarioPerfilMenus);
        _unitOfWork.UsuarioPerfilRepository.EditarUsuarioPerfil(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.Mensagem = "Ocorreu um erro ao salvar o perfil";
      }

      return Json(retorno);
    }

    [HttpPost]
    public ActionResult RemoverPerfil(Guid idUsuarioPerfil)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("perfis"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      var model = _unitOfWork.UsuarioPerfilRepository
        .BuscarPerfil(idUsuarioPerfil);

      _unitOfWork.UsuarioPerfilMenuRepository.DeleteAll(a => a.IDUsuarioPerfil.Equals(idUsuarioPerfil));
      _unitOfWork.UsuarioPerfilRepository.Delete(model);

      var viewModel = Mapper.Map<IEnumerable<UsuarioPerfil>, IEnumerable<UsuarioPerfisViewModel>>
        (_unitOfWork.UsuarioPerfilRepository.BuscarPerfis(UsuarioLogado.SuperUser));

      return PartialView("_PartialGridPerfis", viewModel);
    }

    private List<Menu> ConstruirMenus()
    {
      var menusConstruidos = new List<Menu>();
      var menus = _unitOfWork.MenuRepository
        .BuscarMenus()
        .ToList();

      foreach (var menu in menus)
      {
        var itemMenuConstruido = menu;

        var menusFilhos = menus
          .Where(m => m.IDMenuPai.Equals(menu.IDMenu))
          .ToList();

        foreach (var menuFilho in menusFilhos)
        {
          var subMenusFilhos = menus
            .Where(m => m.IDMenuPai.Equals(menuFilho.IDMenu)
            ).ToList();

          foreach (Menu subMenuFilho in subMenusFilhos)
            menuFilho.MenusFilhos.Add(subMenuFilho);

          itemMenuConstruido.MenusFilhos.Add(menuFilho);
        }

        if (itemMenuConstruido.IDMenuPai == null)
          menusConstruidos.Add(itemMenuConstruido);
      }

      return menusConstruidos;
    }

    private ICollection<UsuarioPerfilMenu> BuscarMenusString(string listaMenusString, Guid idUsuarioPerfil)
    {
      var idsMenus = listaMenusString.Split(';');
      var model = new List<UsuarioPerfilMenu>();

      for (int i = 0; i < idsMenus.Length; i++)
      {
        if (!string.IsNullOrEmpty(idsMenus[i]))
        {
          var menu = new UsuarioPerfilMenu();

          menu.IDUsuarioPerfilMenu = Guid.NewGuid();
          menu.IDUsuarioPerfil = idUsuarioPerfil;
          menu.IDMenu = Guid.Parse(idsMenus[i]);

          model.Add(menu);
        }
      }

      return model;
    }

    #endregion

    #region Usuários

    [HttpGet]
    public ActionResult Usuarios()
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return RedirectToAction("Index", "Principal");

      try
      {
        var model = _unitOfWork
       .UsuarioRepository
       .BuscarUsuarios();

        var usuariosViewModel = Mapper
          .Map<IEnumerable<Usuario>, IEnumerable<UsuariosViewModel>>(model);

        return View(usuariosViewModel);
      }
      catch
      {
        throw;
      }
    }

    [HttpGet]
    public ActionResult Usuario(Guid id)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return RedirectToAction("Index", "Principal");

      var model = _unitOfWork.UsuarioRepository.BuscarUsuario(id);

      if (!UsuarioLogado.SuperUser && model.UsuarioPerfil.SuperUser)
        return RedirectToAction("Usuarios");
      
      var viewModel = Mapper.Map<Usuario, UsuarioViewModel>(model);

      viewModel.ListPerfis = PreencherPerfis();

      return View(viewModel);
    }

    [HttpGet]
    public ActionResult NovoUsuario()
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return RedirectToAction("Index", "Principal");

      var viewModel = new UsuarioViewModel();
      viewModel.ListPerfis = PreencherPerfis();
      
      return View(viewModel);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult CadastrarUsuario(UsuarioViewModel viewModel)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      var retorno = new RetornoJson(false, "Usuário cadastrado com sucesso", Url.Action("Usuarios", "Administracao"));

      try
      {
        viewModel.IDUsuario = Guid.NewGuid();
        viewModel.IDPessoa = Guid.NewGuid();
        viewModel.DataHoraCriacao = DateTime.Now;
        viewModel.Senha = Criptografia.Criptografar(viewModel.Senha, Framework.Enums.TipoCriptografia.BCrypt);

        var model = Mapper.Map<UsuarioViewModel, Usuario>(viewModel);

        model.Pessoa = new Pessoa()
        {
          IDPessoa = viewModel.IDPessoa,
          DataHoraCriacao = DateTime.Now,
          Email = viewModel.Email,
          NomeCompleto = viewModel.Nome
        };

        _unitOfWork.UsuarioRepository.CadastrarUsuario(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.UrlRetorno = string.Empty;
        retorno.Mensagem = "Ocorreu um erro ao cadastrar o usuário";
      }

      return Json(retorno);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult EditarUsuario(UsuarioViewModel viewModel)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      var retorno = new RetornoJson(false, "Usuário editado com sucesso", string.Empty);

      try
      {
        var model = Mapper.Map<UsuarioViewModel, Usuario>(viewModel);
        var pessoa = Mapper.Map<UsuarioViewModel, Pessoa>(viewModel);

        if (!string.IsNullOrEmpty(model.Senha))
          model.Senha = Criptografia.Criptografar(model.Senha, Framework.Enums.TipoCriptografia.BCrypt);

        viewModel.DataHoraAlteracao = DateTime.Now;

        _unitOfWork.PessoaRepository.EditarPessoa(pessoa);
        _unitOfWork.UsuarioRepository.EditarUsuario(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.Mensagem = "Ocorreu um erro ao salvar o usuário";
      }

      return Json(retorno);
    }

    [HttpPost]
    public ActionResult RemoverUsuario(Guid idUsuario)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("usuario"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      if (!UsuarioLogado.IDUsuario.Equals(idUsuario))
        _unitOfWork.UsuarioRepository.RemoverUsuario(idUsuario);

      var viewModel = Mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuariosViewModel>>
        (_unitOfWork.UsuarioRepository.BuscarUsuarios());

      return PartialView("_PartialGridUsuarios", viewModel);
    }

    private SelectList PreencherPerfis()
    {
      return new SelectList(_unitOfWork.UsuarioPerfilRepository
        .BuscarPerfis(UsuarioLogado.SuperUser), "IDUsuarioPerfil", "Nome");
    }

    #endregion
  }
}