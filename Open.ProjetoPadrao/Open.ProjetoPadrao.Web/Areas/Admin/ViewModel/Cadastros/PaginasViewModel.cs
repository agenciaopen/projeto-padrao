﻿using System;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public sealed class PaginasViewModel
  {
    public Guid IDPagina { get; set; }
    public string Nome { get; set; }
    public string Identificador { get; set; }
    public bool PaginaConfigurada { get; set; }
  }
}