﻿using System;
using System.Security.Principal;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Authentication
{
  public interface ICustomUsuario : IPrincipal
  {
    Guid IDUsuario { get; set; }
    Guid IDUsuarioPerfil { get; set; }
    Guid IDPessoa { get; set; }
    string Nome { get; set; }
    string Email { get; set; }
    string Login { get; set; }
    bool Ativo { get; set; }
    bool Deletado { get; set; }
    bool AlterarSenhaPrimeiroAcesso { get; set; }
    bool SuperUser { get; set; }
    bool CadastraPagina { get; set; }
    bool EditaPagina { get; set; }
    DateTime DataHoraPrimeiroAcesso { get; set; }
    List<Menu> MenusPermitidos { get; set; }
    string ActionsPermitidas { get; set; }
  }
}