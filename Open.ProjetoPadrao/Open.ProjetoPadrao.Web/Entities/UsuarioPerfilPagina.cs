﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("UsuarioPerfilPagina")]
  public class UsuarioPerfilPagina
  {
    [Key]
    public Guid IDUsuarioPerfilPagina { get; set; }

    [Required]
    public Guid IDUsuarioPerfil { get; set; }

    [Required]
    public Guid IDPagina { get; set; }

    public virtual UsuarioPerfil UsuarioPerfil { get; set; }

    public virtual Pagina Pagina { get; set; }
  }
}