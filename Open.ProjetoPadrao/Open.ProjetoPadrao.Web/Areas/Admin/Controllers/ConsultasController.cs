﻿using AutoMapper;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.ViewModels.Admin;
using Open.ProjetoPadrao.Web.Enums;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class ConsultasController : BaseController
  {
    public ConsultasController(IUnitOfWork unitOfWork) : base(unitOfWork) { }

    #region Contatos

    [HttpGet]
    public ActionResult Contatos()
    {
      return View(BuscarContatos());
    }

    [HttpPost]
    public ActionResult RemoverContato(Guid idContato)
    {
      _unitOfWork.ContatoRepository.RemoverContato(idContato);
      return PartialView("_PartialGridContatos", BuscarContatos());
    }

    [HttpGet]
    public ActionResult Contato(Guid id)
    {
      _unitOfWork.ContatoRepository.AtualizarContatoVisualizado(id);
      var model = _unitOfWork.ContatoRepository.CarregarContato(id);

      var viewModel = Mapper.Map<Contato, ContatoViewModel>(model);

      switch (model.TipoContato)
      {
        case TipoContato.Contato:
          viewModel.ObjContato = BindMapperViewModel<ContatoGeralViewModel>(model);
          break;
      }

      return View(viewModel);
    }

    private T BindMapperViewModel<T>(Contato contato) where T : class
    {
      return Mapper.Map<Contato, T>(contato);
    }

    private IEnumerable<ContatosViewModel> BuscarContatos()
    {
      return Mapper
       .Map<IEnumerable<Contato>, IEnumerable<ContatosViewModel>>
       (_unitOfWork.ContatoRepository.BuscarContatos());
    }

    #endregion

    #region Newsletter

    [HttpGet]
    public ActionResult Newsletters()
    {
      return View(BuscarNewsletter());
    }

    [HttpPost]
    public ActionResult RemoverNewsletter(Guid idNewsletter)
    {
      _unitOfWork.NewsletterRepository.RemoverNewsletter(idNewsletter);
      return PartialView("_PartialGridNewsletters", BuscarNewsletter());
    }

    private IEnumerable<NewsletterViewModel> BuscarNewsletter()
    {
      return Mapper
       .Map<IEnumerable<Newsletter>, IEnumerable<NewsletterViewModel>>(_unitOfWork.NewsletterRepository.ListarNewsletters());
    }

    #endregion
  }
}