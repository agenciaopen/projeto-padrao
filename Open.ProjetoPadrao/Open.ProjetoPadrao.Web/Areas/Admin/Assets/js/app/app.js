$.validator.methods.range = function (value, element, param) { var globalizedValue = value.replace(".", ""); globalizedValue = globalizedValue.replace(",", "."); return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]); };
$.validator.methods.number = function (value, element) { return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value); };
$.validator.methods.date = function (value, element) { return this.optional(element) || Globalize.parseDate(value, 'dd/MM/yyyy') !== null; }

function ValidaListaGuid(s) {
  if (s == "00000000-0000-0000-0000-000000000000")
    return false;

  return true;
}

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

function inicioCadastro() {
  $("#btnCadastrar").button('loading');
}

function sucessoCadastro(retorno) {

  $("#btnCadastrar").button('reset');

  if (retorno.Erro) {
    exibirMensagem("E", retorno.Mensagem);
  }
  else {
    exibirMensagem("S", retorno.Mensagem);

    if (retorno.UrlRetorno != "") {
      exibirMensagem("I", "Redirecionando...");
      window.location = retorno.UrlRetorno;
    }
  }
}

function falhaCadastro(retorno) {
  $("#btnCadastrar").button('reset');
  exibirMensagem("E", retorno.Mensagem);
}

function exibirMensagem(tipoMensagem, mensagem) {
  if (tipoMensagem == 'S')
    toastr["success"](mensagem);
  else if (tipoMensagem == 'E')
    toastr["error"](mensagem);
  else if (tipoMensagem == 'I')
    toastr["info"](mensagem);
  else if (tipoMensagem == 'A')
    toastr["warning"](mensagem);
}

function carregarDataTables() {
  $("[data-tipo='datatable']").DataTable({
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
    language: {
      processing: "Carregando...",
      search: "Pesquisar&nbsp;:",
      lengthMenu: "Exibir _MENU_ Itens",
      info: "Exibindo de _START_ at&eacute; _END_ de _TOTAL_ itens",
      infoEmpty: "Nenhum item encontrado",
      infoFiltered: "(Filtrado de _MAX_ no total)",
      infoPostFix: "",
      loadingRecords: "Carregando...",
      zeroRecords: "Nenhum item encontrado",
      emptyTable: "Nenhum item encontrado",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Pr&oacute;ximo",
        last: "&Uacute;ltimo"
      },
      aria: {
        sortAscending: ": organiza por ordem crescente",
        sortDescending: ": organiza por ordem decrescente"
      }
    }
  });
}

function exibirAlerta(tipoAlerta, mensagem) {
  if (tipoAlerta == 'E')
    swal("Oops!", mensagem, "error");
}

$(document).ready(function () {

  carregarDataTables();

  $('input[data-mascara="RG"]').inputmask("AA-99.999.999");
  $('input[data-mascara="Telefone"]').inputmask({ mask: ["(99) 9999-9999", "(99) 99999-9999"] });
  $('input[data-mascara="CEP"]').inputmask("99.999-999");
  $('input[data-mascara="CPF"]').inputmask("999.999.999-99");
  $('input[data-mascara="CNPJ"]').inputmask("99.999.999/9999-99");
  $('input[data-mascara="InscricaoEstadual"]').inputmask("9999999999.99-99");
  $('input[data-mascara="InscricaoMunicipal"]').inputmask("0.999.999/999-(9)|(A)");
  $('input[data-mascara="SerieCTPS"]').inputmask("999-9");
  $('input[data-mascara="PIS"]').inputmask("999.99999.99-9");
  $('input[data-mascara="IP"]').inputmask("ip");
  $('input[data-mascara="NumeroInteiro"]').inputmask("integer", { rightAlign: false });
  $('input[data-mascara="NumeroDecimal"]').inputmask({ regex: "[0-9]{1,6},[0-9]{0,3}", autoGroup: true, groupSeparator: ",", groupSize: 3 });
  $('input[data-mascara="NumeroDecimalCaixa"]').inputmask("decimal", { digits: 3, radixPoint: ".", autoGroup: false, groupSeparator: ".", groupSize: 3, rightAlign: false });
  $('input[data-mascara="NumeroDimensao"]').inputmask("decimal", { digits: 2, radixPoint: ".", autoGroup: true, groupSeparator: ",", groupSize: 2, rightAlign: false });
  $('input[data-mascara="NumeroPorcentagem"]').inputmask("9[9],9[9]");
  $('input[data-mascara="Dinheiro"]').inputmask("currency", { prefix: '', radixPoint: ",", groupSeparator: "", autoGroup: true, rightAlign: false });
  $('input[data-mascara="Data"]').inputmask("99/99/9999", { "placeholder": "__/__/____" });
  $('input[data-mascara="Hora"]').inputmask('h:s', { "placeholder": "__:__" });

  $('input[data-componente="DataMinHoje"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: 'd',
    language: 'pt-BR'
  });

  $('input[data-componente="Data"]').datepicker({
    format: 'dd/mm/yyyy',
    language: 'pt-BR'
  });

  $('input[data-componente="DataMaxHoje"]').datepicker({
    format: 'dd/mm/yyyy',
    endDate: 'd',
    language: 'pt-BR'
  });
  
  if ($(this).width() < 769) {
    $('body').addClass('body-small')
  } else {
    $('body').removeClass('body-small')
  }

  // MetsiMenu
  $('#side-menu').metisMenu();

  // Collapse ibox function
  $('.collapse-link').click(function () {
    var ibox = $(this).closest('div.ibox');
    var button = $(this).find('i');
    var content = ibox.children('.ibox-content');
    content.slideToggle(200);
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    ibox.toggleClass('').toggleClass('border-bottom');
    setTimeout(function () {
      ibox.resize();
      ibox.find('[id^=map-]').resize();
    }, 50);
  });

  // Close ibox function
  $('.close-link').click(function () {
    var content = $(this).closest('div.ibox');
    content.remove();
  });

  // Fullscreen ibox function
  $('.fullscreen-link').click(function () {
    var ibox = $(this).closest('div.ibox');
    var button = $(this).find('i');
    $('body').toggleClass('fullscreen-ibox-mode');
    button.toggleClass('fa-expand').toggleClass('fa-compress');
    ibox.toggleClass('fullscreen');
    setTimeout(function () {
      $(window).trigger('resize');
    }, 100);
  });

  // Close menu in canvas mode
  $('.close-canvas-menu').click(function () {
    $("body").toggleClass("mini-navbar");
    SmoothlyMenu();
  });

  // Run menu of canvas
  $('body.canvas-menu .sidebar-collapse').slimScroll({
    height: '100%',
    railOpacity: 0.9
  });

  // Open close right sidebar
  $('.right-sidebar-toggle').click(function () {
    $('#right-sidebar').toggleClass('sidebar-open');
  });

  // Initialize slimscroll for right sidebar
  $('.sidebar-container').slimScroll({
    height: '100%',
    railOpacity: 0.4,
    wheelStep: 10
  });

  // Open close small chat
  $('.open-small-chat').click(function () {
    $(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
    $('.small-chat-box').toggleClass('active');
  });

  // Initialize slimscroll for small chat
  $('.small-chat-box .content').slimScroll({
    height: '234px',
    railOpacity: 0.4
  });

  // Small todo handler
  $('.check-link').click(function () {
    var button = $(this).find('i');
    var label = $(this).next('span');
    button.toggleClass('fa-check-square').toggleClass('fa-square-o');
    label.toggleClass('todo-completed');
    return false;
  });

  // Minimalize menu
  $('.navbar-minimalize').on('click', function (event) {
    event.preventDefault();
    $("body").toggleClass("mini-navbar");
    SmoothlyMenu();

  });

  // Tooltips demo
  $('.tooltip-demo').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
  });

  // Move modal to body
  // Fix Bootstrap backdrop issu with animation.css
  $('.modal').appendTo("body");

  // Full height of sidebar
  function fix_height() {
    var heightWithoutNavbar = $("body > #wrapper").height() - 61;
    $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

    var navbarHeight = $('nav.navbar-default').height();
    var wrapperHeight = $('#page-wrapper').height();

    if (navbarHeight > wrapperHeight) {
      $('#page-wrapper').css("min-height", navbarHeight + "px");
    }

    if (navbarHeight < wrapperHeight) {
      $('#page-wrapper').css("min-height", $(window).height() + "px");
    }

    if ($('body').hasClass('fixed-nav')) {
      if (navbarHeight > wrapperHeight) {
        $('#page-wrapper').css("min-height", navbarHeight + "px");
      } else {
        $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
      }
    }

  }

  fix_height();

  // Fixed Sidebar
  $(window).bind("load", function () {
    if ($("body").hasClass('fixed-sidebar')) {
      $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
      });
    }
  });

  // Move right sidebar top after scroll
  $(window).scroll(function () {
    if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
      $('#right-sidebar').addClass('sidebar-top');
    } else {
      $('#right-sidebar').removeClass('sidebar-top');
    }
  });

  $(window).bind("load resize scroll", function () {
    if (!$("body").hasClass('body-small')) {
      fix_height();
    }
  });

  $("[data-toggle=popover]")
    .popover();

  // Add slimscroll to element
  $('.full-height-scroll').slimscroll({
    height: '100%'
  })
});

$(window).bind("resize", function () {
  if ($(this).width() < 769) {
    $('body').addClass('body-small')
  } else {
    $('body').removeClass('body-small')
  }
});

$(document).ready(function () {
  if (localStorageSupport) {

    var collapse = localStorage.getItem("collapse_menu");
    var fixedsidebar = localStorage.getItem("fixedsidebar");
    var fixednavbar = localStorage.getItem("fixednavbar");
    var boxedlayout = localStorage.getItem("boxedlayout");
    var fixedfooter = localStorage.getItem("fixedfooter");

    var body = $('body');

    if (fixedsidebar == 'on') {
      body.addClass('fixed-sidebar');
      $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
      });
    }

    if (collapse == 'on') {
      if (body.hasClass('fixed-sidebar')) {
        if (!body.hasClass('body-small')) {
          body.addClass('mini-navbar');
        }
      } else {
        if (!body.hasClass('body-small')) {
          body.addClass('mini-navbar');
        }

      }
    }

    if (fixednavbar == 'on') {
      $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
      body.addClass('fixed-nav');
    }

    if (boxedlayout == 'on') {
      body.addClass('boxed-layout');
    }

    if (fixedfooter == 'on') {
      $(".footer").addClass('fixed');
    }
  }
});

function localStorageSupport() {
  return (('localStorage' in window) && window['localStorage'] !== null)
}

function animationHover(element, animation) {
  element = $(element);
  element.hover(
    function () {
      element.addClass('animated ' + animation);
    },
    function () {
      //wait for animation to finish before removing classes
      window.setTimeout(function () {
        element.removeClass('animated ' + animation);
      }, 2000);
    });
}

function SmoothlyMenu() {
  if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
    // Hide menu in order to smoothly turn on when maximize menu
    $('#side-menu').hide();
    // For smoothly turn on menu
    setTimeout(
      function () {
        $('#side-menu').fadeIn(400);
      }, 200);
  } else if ($('body').hasClass('fixed-sidebar')) {
    $('#side-menu').hide();
    setTimeout(
      function () {
        $('#side-menu').fadeIn(400);
      }, 100);
  } else {
    // Remove all inline style from jquery fadeIn function to reset menu state
    $('#side-menu').removeAttr('style');
  }
}

function WinMove() {
  var element = "[class*=col]";
  var handle = ".ibox-title";
  var connect = "[class*=col]";
  $(element).sortable(
    {
      handle: handle,
      connectWith: connect,
      tolerance: 'pointer',
      forcePlaceholderSize: true,
      opacity: 0.8
    })
    .disableSelection();
}
