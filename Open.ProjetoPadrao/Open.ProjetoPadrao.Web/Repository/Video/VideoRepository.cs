﻿using System;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class VideoRepository : RepositoryBase<Video>, IVideoRepository
  {
    private readonly Context _context;

    public VideoRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Video> ListarVideos(int? qtdRegistos, bool apenasHome = false)
    {
      var model = Get();

      if (apenasHome)
        model = model.Where(a => a.ExibirHome).ToList();

      if (qtdRegistos.HasValue)
        model = model.Take(qtdRegistos.Value).ToList();

      return model.OrderByDescending(a => a.DataCriacao);
    }
    public IEnumerable<Video> BuscarVideos()
    {
      return Get()
        .OrderByDescending(a => a.DataCriacao);
    }
    public Video BuscarVideo(Guid idVideo)
    {
      return Get(a => a.IDVideo.Equals(idVideo))
        .FirstOrDefault();
    }
    public void CadastrarVideo(Video model)
    {
      _context.Videos.Add(model);
      _context.SaveChanges();
    }
    public void EditarVideo(Video model)
    {
      Edit(model, true);
      _context.SaveChanges();
    }
    public void RemoverVideo(Guid idVideo)
    {
      var model = BuscarVideo(idVideo);

      _context.Videos.Remove(model);
      _context.SaveChanges();
    }
  }
}