﻿using System.Web.Mvc;
using Open.ProjetoPadrao.Web.Authentication;

namespace Open.ProjetoPadrao.Web.Areas.Admin
{
  public abstract class BaseViewPage : WebViewPage
  {
    public virtual UsuarioPrincipal UsuarioLogado
    {
      get { return base.User as UsuarioPrincipal; }
    }
  }

  public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
  {
    public virtual UsuarioPrincipal UsuarioLogado
    {
      get { return base.User as UsuarioPrincipal; }
    }
  }
}