﻿using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Open.ProjetoPadrao.Web.Classes
{
  public class MultiLanguageViewEngine : RazorViewEngine
  {
    private static string _currentCulture = GlobalHelper.CurrentCulture;

    public MultiLanguageViewEngine() : this(GlobalHelper.CurrentCulture) { }

    public MultiLanguageViewEngine(string idioma)
    {
      SetCurrentCulture(idioma);
    }

    public void SetCurrentCulture(string idioma)
    {
      _currentCulture = idioma;
      ICollection<string> arViewLocationFormats = new string[] { "~/Views/{1}/" + idioma + "/{0}.cshtml" };
      ICollection<string> arBaseViewLocationFormats = new string[] {
                @"~/Views/{1}/{0}.cshtml",
                @"~/Views/Shared/{0}.cshtml"};
      ViewLocationFormats = arViewLocationFormats.Concat(arBaseViewLocationFormats).ToArray();
    }

    public static string CurrentCulture
    {
      get { return _currentCulture; }
    }
  }
}