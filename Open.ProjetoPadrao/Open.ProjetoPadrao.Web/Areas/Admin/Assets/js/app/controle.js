﻿var table;

$(document).on("click", "#btnCancelarEdicaoControle", function () {
  window.location = $("#PaginaRetorno").val();
});
$(document).on("click", "#btnSalvarEdicaoHeadline", function () {
  var $this = $(this);
  var urlAction = $this.data("urlaction");
  var idPaginaControle = $("#IDPaginaControle").val();
  var textoHeadline = $("#txtTextoHeadline").val();
  var textoHeadlineEN = $("#txtTextoHeadlineEN").val();
  var identificadorControle = $("#txtIdentificadorControle").val();

  $this.button('loading');

  $.ajax({
    url: urlAction,
    type: "POST",
    data: { idPaginaControle: idPaginaControle, textoHeadline: textoHeadline, textoHeadlineEN: textoHeadlineEN, identificador: identificadorControle },
    success: function () {
      $this.button('reset');
      exibirMensagem("S", "Headline atualizada com sucesso!");
    },
    error: function (xhr, status, error) {
      $this.button('reset');
      exibirAlerta("E", "Ocorreu um erro ao atualizar os detalhes do controle");
    }
  });
});
$(document).on("click", "#btnSalvarEdicaoTexto", function () {
  var $this = $(this);
  var urlAction = $this.data("urlaction");
  var idPaginaControle = $("#IDPaginaControle").val();
  var conteudoTexto = $("#txtConteudoTexto").val();
  var conteudoTextoEN = $("#txtConteudoTextoEN").val();
  var identificadorControle = $("#txtIdentificadorControle").val();

  $this.button('loading');

  $.ajax({
    url: urlAction,
    type: "POST",
    data: { idPaginaControle: idPaginaControle, conteudoTexto: conteudoTexto, conteudoTextoEN: conteudoTextoEN, identificador: identificadorControle },
    success: function () {
      $this.button('reset');
      exibirMensagem("S", "Texto atualizado com sucesso!");
    },
    error: function (xhr, status, error) {
      $this.button('reset');
      exibirAlerta("E", "Falha ao atualizar os detalhes do controle");
    }
  });
});
$(document).on("click", "#btnSalvarEdicaoTitleDescription", function () {
  var $this = $(this);
  var urlAction = $this.data("urlaction");
  var idPaginaControle = $("#IDPaginaControle").val();
  var conteudoTitle = $("#txtConteudoTitle").val();
  var conteudoTitleEN = $("#txtConteudoTitleEN").val();
  var conteudoDescription = $("#txtConteudoDescription").val();
  var conteudoDescriptionEN = $("#txtConteudoDescriptionEN").val();
  var identificadorControle = $("#txtIdentificadorControle").val();
  
  if (identificadorControle == '') {
    exibirMensagem("A", "Informe o identificador do controle para salvar");
  }
  else {
    $this.button('loading');

    $.ajax({
      url: urlAction,
      type: "POST",
      data: { idPaginaControle: idPaginaControle, conteudoTitle: conteudoTitle, conteudoDescription: conteudoDescription, conteudoTitleEN: conteudoTitleEN, conteudoDescriptionEN: conteudoDescriptionEN, identificador: identificadorControle },
      success: function () {
        $this.button('reset');
        exibirMensagem("S", "Title e Description atualizados com sucesso!");
      },
      error: function (xhr, status, error) {
        $this.button('reset');
        exibirAlerta("E", "Falha ao atualizar os detalhes do controle");
      }
    });
  }
});
$(document).on("click", "#btnSalvarIdentificadorGaleria", function () {
  var $this = $(this);
  var urlAction = $this.data("url");
  var idPaginaControle = $("#IDPaginaControle").val();
  var identificadorControle = $("#txtIdentificadorControle").val();

  $this.button('loading');

  $.ajax({
    url: urlAction,
    type: "POST",
    data: { idPaginaControle: idPaginaControle, identificadorControle: identificadorControle },
    success: function () {
      $this.button('reset');
      exibirMensagem("S", "Identificador do controle salvo com sucesso!");
    },
    error: function (xhr, status, error) {
      $this.button('reset');
      exibirAlerta("E", "Ocorreu um erro ao atualizar os detalhes do controle");
    }
  });
});
$(document).on("click", "[data-id='btnRemoverImagemGaleria']", function () {

  var $this = $(this);
  var idImagem = $this.data("idimagem");
  var idPaginaControle = $("#IDPaginaControle").val();
  var url = $("#urlRemoverImagemGaleria").val();
  var $divPai = $this.closest("[data-id='divPai']");

  swal({
    text: "Deseja remover esta imagem?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {

        $this.button('loading');

        $.ajax({
          url: url,
          type: "POST",
          data: { idPaginaControle: idPaginaControle, idPaginaControleImagem: idImagem },
          success: function (partial) {
            $divPai.html(partial);
            $this.button('reset');
            carregarTabelaGaleria();
          },
          error: function (xhr, status, error) {
            exibirMensagem("E", "Falha ao remover a imagem");
            $this.button('reset');
          }
        });
      }
    });

});
$(document).on("click", "[data-id='btnSalvarImagemGaleria']", function () {

  var $this = $(this);
  var idImagem = $this.data("idimagem");
  var url = $("#urlSalvarImagemGaleria").val();
  var alt = $("[data-id='txtAlt-" + idImagem + "']").val();
  //var legenda = $("[data-id='txtLegenda-" + idImagem + "']").val();
  //var titulo = $("[data-id='txtTitulo-" + idImagem + "']").val();
  //var subtitulo = $("[data-id='txtSubTitulo-" + idImagem + "']").val();
  var urlRedirecionamento = $("[data-id='txtURLRedirecionamento-" + idImagem + "']").val();
  var altEN = $("[data-id='txtAltEN-" + idImagem + "']").val();
  //var legendaEN = $("[data-id='txtLegendaEN-" + idImagem + "']").val();
  //var tituloEN = $("[data-id='txtTituloEN-" + idImagem + "']").val();
  //var subtituloEN = $("[data-id='txtSubTituloEN-" + idImagem + "']").val();
  var urlRedirecionamentoEN = $("[data-id='txtURLRedirecionamentoEN-" + idImagem + "']").val();

  if (alt.trim() === '' || altEN.trim() === '') {
    exibirMensagem("A", "Informe o ALT da imagem para salvar (Português e Inglês)");
  }
  else {
    $this.button('loading');

    $.ajax({
      url: url,
      type: "POST",
      data: { idPaginaControleImagem: idImagem, alt: alt, title: "", legenda: "", titulo: "", subTitulo: "", urlRedirecionamento: urlRedirecionamento, altEN: altEN, titleEN: "", legendaEN: "", tituloEN: "", subtituloEN: "", urlRedirecionamentoEN: urlRedirecionamentoEN },
      success: function (retorno) {
        $this.button('reset');

        if (retorno.Erro)
          exibirMensagem("E", retorno.Mensagem);
        else
          exibirMensagem("S", retorno.Mensagem);
      },
      error: function (xhr, status, error) {
        $this.button('reset');
        exibirMensagem("E", "Ocorreu um erro ao salvar os detalhes da imagem");
      }
    });
  }

});

Dropzone.autoDiscover = false;

$(document).ready(function () {
  var urlEnviar = $("#urlEnvioImagensGaleria").val();
  var urlRemover = $("#urlRemoverImagemGaleria").val();

  $('[data-tipo="summernote"]').summernote({
    lang: 'pt-BR',
    callbacks: {
      // remove a formatação quando o conteúdo é colado no editor
      onPaste: function (e) {
        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
        e.preventDefault();
        setTimeout(function () {
          document.execCommand('insertText', false, bufferText);
        }, 10);
      }
    }
  });

  $("#divControleImagens").dropzone(
    {
      url: urlEnviar,
      acceptedFiles: '.jpg,.jpeg,.png',
      maxFilesize: 2,
      dictFallbackMessage: 'Seu navegador não suporta envio de arquivos por "drag-n-drop".',
      dictFallbackText: '',
      dictDefaultMessage: "Clique aqui ou arraste imagens para esta área",
      dictFileTooBig: "O tamanho máximo permitido é de 2mb por arquivo",
      dictInvalidFileType: "Tipo de arquivo não permitido. Utilize apenas imagens",
      dictCancelUpload: 'Cancelar envio',
      dictRemoveFile: 'Remover',
      addRemoveLinks: true,
      complete: function (arquivo) {
        this.removeFile(arquivo);
        recarregarImagensGaleria("#detalhesControleImagens");
      },
      error: function (arquivo, mensagem) {
        this.removeFile(arquivo);
        exibirMensagem("E", mensagem);
      }
    });

  carregarTabelaGaleria();
  
});

function recarregarImagensGaleria(idDivPai) {
  $.ajax({
    url: $("#urlRecarregarImagensGaleria").val(),
    type: "POST",
    data: { idPaginaControle: $("#IDPaginaControle").val() },
    success: function (partial) {
      $(idDivPai).html(partial);
      carregarTabelaGaleria();
    },
    error: function (xhr, status, error) {
      exibirMensagem("E", "Ocorreu um erro ao recarregar a lista de imagens adicionadas");
    }
  });
}
function carregarTabelaGaleria() {
  table = $("#listaGaleriaImagens").dataTable({
    rowReorder: true,
    "ordering": false,
    language: {
      processing: "Carregando...",
      search: "Pesquisar&nbsp;:",
      lengthMenu: "Exibir _MENU_ Itens",
      info: "Exibindo de _START_ at&eacute; _END_ de _TOTAL_ itens",
      infoEmpty: "Nenhum item encontrado",
      infoFiltered: "(Filtrado de _MAX_ no total)",
      infoPostFix: "",
      loadingRecords: "Carregando...",
      zeroRecords: "Nenhum item encontrado",
      emptyTable: "Nenhum item encontrado",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Pr&oacute;ximo",
        last: "&Uacute;ltimo"
      },
      aria: {
        sortAscending: ": organiza por ordem crescente",
        sortDescending: ": organiza por ordem decrescente"
      }
    }
  });
}

$('#listaGaleriaImagens').on('row-reorder.dt', function (dragEvent, data, nodes) {
  var mensagemSucessoExibida = false;
  var mensagemErroExibida = false;

  for (var i = 0, ien = data.length; i < ien; i++) {
    $.ajax({
      type: "POST",
      url: $("#UrlReordenarGaleria").val(),
      data: { idPaginaControleImagem: $(data[i].node)[0].id, novaOrdem: data[i].newPosition },
      success: function () {
        if (!mensagemSucessoExibida) {
          exibirMensagem("S", "Ordenação salva com suceso!");
          mensagemSucessoExibida = true;
        }
      },
      error: function (xhr, status, error) {
        if (!mensagemErroExibida) {
          exibirMensagem("E", "Falha ao salvar a ordenação");
          mensagemErroExibida = true;
        }
      }
    });
  }
});