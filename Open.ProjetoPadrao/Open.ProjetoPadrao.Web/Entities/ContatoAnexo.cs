﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("ContatoAnexo")]
  public class ContatoAnexo
  {
    [Key]
    public Guid IDContatoAnexo { get; set; }

    public Guid IDContato { get; set; }

    public Guid IDArquivo { get; set; }

    public virtual Arquivo Arquivo { get; set; }

    public virtual Contato Contato { get; set; }
  }
}