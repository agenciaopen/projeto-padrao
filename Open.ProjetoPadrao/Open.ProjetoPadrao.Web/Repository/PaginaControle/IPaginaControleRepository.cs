﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IPaginaControleRepository : IRepositoryBase<PaginaControle>
  {
    IEnumerable<PaginaControle> BuscarControlesPagina(Guid idPagina);
    void AdicionarControlePagina(PaginaControle model);
    void RemoverTodosControlesPagina(Guid idPagina);
    PaginaControle BuscarControlePagina(Guid idPaginaControle);
    void AtualizarConfiguracaoControlePagina(Guid idPaginaControle, string identificador);
    Guid RemoverControlePagina(Guid idPaginaControle);
  }
}