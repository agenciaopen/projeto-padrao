﻿$(document).on("click", "[data-id='btnRemoverUsuario']", function (e) {
  var $this = $(this);
  var idUsuario = $this.data("idusuario");
  var url = $("#urlRemoverUsuario").val();

  console.log(url);

  swal({
    text: "Deseja remover este usuário e todas as suas associações?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {
        $this.button('loading');
        $.ajax({
          url: url,
          type: "POST",
          data: { idUsuario: idUsuario },
          success: function (partial) {
            $("#divGridUsuarios").html(partial);
            $this.button('reset');
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            exibirMensagem("E", "Falha ao remover o usuário");
            $this.button('reset');
          }
        });
      }
    });
});