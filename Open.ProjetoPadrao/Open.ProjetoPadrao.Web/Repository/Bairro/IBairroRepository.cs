﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IBairroRepository : IRepositoryBase<Bairro>
  {
    IEnumerable<Bairro> BuscarBairros();
    IEnumerable<Bairro> BuscarBairros(Guid idCidade);
    Bairro BuscarBairro(Guid idBairro);
    void CadastrarBairro(Bairro model);
    void EditarBairro(Guid idBairro, Guid idCidade, string nome);
    void RemoverBairro(Guid idBairro);
  }
}