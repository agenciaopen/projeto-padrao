﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("PaginaControleParametro")]
  public class PaginaControleParametro
  {
    [Key]
    public Guid IDPaginaControleParametro { get; set; }

    public Guid IDPaginaControle { get; set; }

    public string ConteudoTexto { get; set; }

    public string ConteudoHeadline { get; set; }

    public string ConteudoTitle { get; set; }

    public string ConteudoDescription { get; set; }

    public string ConteudoTextoEN { get; set; }

    public string ConteudoHeadlineEN { get; set; }

    public string ConteudoTitleEN { get; set; }

    public string ConteudoDescriptionEN { get; set; }

    public virtual PaginaControle PaginaControle { get; set; }
  }
}