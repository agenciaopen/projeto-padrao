﻿using System.Web;
using System.Web.Routing;

namespace Open.ProjetoPadrao.Web.Constraints
{
  public class LanguageRouteConstraint : IRouteConstraint
  {
    public string Idioma { get; set; }

    public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
    {
      return values["idioma"].ToString() == Idioma;
    }
  }
}