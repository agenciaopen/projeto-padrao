﻿$(document).on("click", "[data-id='btnRemoverPerfil']", function (e) {
  var $this = $(this);
  var idPerfil = $this.data("idperfil");
  var url = $("#urlRemoverPerfil").val();

  swal({
    text: "Deseja remover este perfil e todas as suas associações?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {
        $this.button('loading');
        $.ajax({
          url: url,
          type: "POST",
          data: { idUsuarioPerfil: idPerfil },
          success: function (partial) {
            $this.button('reset');
            $("#divGridPerfis").html(partial);
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirMensagem("E", "Falha ao remover o perfil");
          }
        });
      }
    });
});
$(document).on("change", "[data-id='checkPerfilPai']", function (e) {
  $(this).closest('[data-id="conjuntoChecks"]').find(':checkbox').prop('checked', this.checked);
});
$(document).on("change", "[data-id='checkPerfilFilho']", function (e) {
  existeFilhoMarcado = false;

  idFilho = $(this).val();
  marcado = $(this).prop("checked");
  filhos = $(this).closest('[data-id="conjuntoChecks"]').find('[data-id="checkPerfilFilho"]');
  subFilhos = $(this).closest('[data-id="conjuntoChecks"]').find('[data-id="checkPerfilSubFilho"]');

  $(filhos).each(function (i, v) {
    if ($(v).prop("checked")) {
      existeFilhoMarcado = true;
    }
  });
  $(subFilhos).each(function (x, s) {
    if ($(s).attr("name") == idFilho) {
      $(s).prop("checked", marcado);
    }
  });
  $(this).closest('[data-id="conjuntoChecks"]').find('[data-id="checkPerfilPai"]').prop('checked', existeFilhoMarcado);
});
$(document).on("change", "#SuperUser", function (e) {
  var valorInput = $(this).val();
  var superUser = valorInput == "True" ? true : false;

  $("[data-id='checkPermissao'][value='" + valorInput + "']").prop("checked", superUser);
  $("[data-id='checkPermissao']").prop("disabled", superUser ? "disabled" : "");
});

$("#formPerfil").submit(function () {

  $("[data-id='checkPermissao']").removeAttr("disabled");
  $("#ListaMenusString").val("");

  checkMenus = $('[data-toggle="chkMenu"]');

  $(checkMenus).each(function (i, v) {

    $menu = $(v);

    if ($menu.prop("checked")) {
      idMenu = $menu.attr("value");
      menusAtuais = $("#ListaMenusString").val();
      $("#ListaMenusString").val(menusAtuais + ';' + idMenu);
    }
  });
  
});