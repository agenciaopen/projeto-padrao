﻿using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class ContatoViewModel
  {
    public object ObjContato { get; set; }
    public string Origem { get; set; }
    public TipoContato TipoContato { get; set; }
  }
}