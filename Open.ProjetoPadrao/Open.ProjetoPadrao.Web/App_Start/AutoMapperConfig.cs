﻿using AutoMapper;
using Open.ProjetoPadrao.Web.Mappers;
using Open.ProjetoPadrao.Web.Mappers.Admin;

namespace Open.ProjetoPadrao.Web
{
  public class AutoMapperConfig
  {
    public static void RegisterMappings()
    {
      Mapper.Initialize(x =>
      {
        x.AddProfile<EntityMappingProfile>();
        x.AddProfile<ViewModelMappingProfile>();

        x.AddProfile<EntityToViewModelMappingAdmin>();
        x.AddProfile<ViewModelToEntityMappingAdmin>();
      });
    }
  }
}