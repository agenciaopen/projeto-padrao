﻿namespace Open.ProjetoPadrao.Web.Enums
{
  public enum TiposControle
  {
    Headline = 1,
    Texto = 2,
    Vitrine = 3,
    Galeria = 4,
    TitleDescription = 5
  }
}