﻿using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class ContatoAnexoRepository : RepositoryBase<ContatoAnexo>, IContatoAnexoRepository
  {
    private readonly Context _context;

    public ContatoAnexoRepository(Context context) : base(context)
    {
      _context = context;
    }
  }
}