﻿using Open.ProjetoPadrao.Web.Repository;

namespace Open.ProjetoPadrao.Web.UnitOfWork
{
  public interface IUnitOfWork
  {
    // Padrões

    IMenuRepository MenuRepository { get; set; }
    IArquivoRepository ArquivoRepository { get; set; }
    IPessoaRepository PessoaRepository { get; set; }
    IUsuarioRepository UsuarioRepository { get; set; }
    IUsuarioPerfilRepository UsuarioPerfilRepository { get; set; }
    IUsuarioPerfilMenuRepository UsuarioPerfilMenuRepository { get; set; }
    IUsuarioPerfilPaginaRepository UsuarioPerfilPaginaRepository { get; set; }
    IPaginaControleRepository PaginaControleRepository { get; set; }
    IPaginaControleImagemRepository PaginaControleImagemRepository { get; set; }
    IPaginaControleParametroRepository PaginaControleParametroRepository { get; set; }
    IPaginaRepository PaginaRepository { get; set; }
    IControleRepository ControleRepository { get; set; }
    IContatoRepository ContatoRepository { get; set; }
    IContatoAnexoRepository ContatoAnexoRepository { get; set; }
    IEstadoRepository EstadoRepository { get; set; }
    ICidadeRepository CidadeRepository { get; set; }
    INewsletterRepository NewsletterRepository { get; set; }
    IEnderecoRepository EnderecoRepository { get; set; }
    IBairroRepository BairroRepository { get; set; }
    IVideoRepository VideoRepository { get; set; }
    IRedirecionamentoSEORepository RedirecionamentoSEORepository { get; set; }


    // Customizados do Cliente

    // Aqui vão os mapeamentos customizados do projeto do cliente

    void Dispose();
    void Commit();
  }
}