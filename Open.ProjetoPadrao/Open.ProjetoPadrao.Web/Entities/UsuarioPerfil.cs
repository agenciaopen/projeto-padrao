﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("UsuarioPerfil")]
  public class UsuarioPerfil
  {
    public UsuarioPerfil()
    {
      Usuarios = new List<Usuario>();
      UsuarioPerfilMenus = new List<UsuarioPerfilMenu>();
      UsuarioPerfilPaginas = new List<UsuarioPerfilPagina>();
    }

    [Key]
    public Guid IDUsuarioPerfil { get; set; }

    [Required, MaxLength(255)]
    public string Nome { get; set; }

    [Required]
    public bool SuperUser { get; set; }

    [Required]
    public bool AcessoSistemaAdmin { get; set; }

    [Required]
    public bool CadastraPagina { get; set; }

    [Required]
    public bool EditaPagina { get; set; }

    [Required]
    public DateTime DataHoraCriacao { get; set; }

    public DateTime? DataHoraAlteracao { get; set; }

    public virtual ICollection<Usuario> Usuarios { get; set; }

    public virtual ICollection<UsuarioPerfilMenu> UsuarioPerfilMenus { get; set; }

    public virtual ICollection<UsuarioPerfilPagina> UsuarioPerfilPaginas { get; set; }
  }
}