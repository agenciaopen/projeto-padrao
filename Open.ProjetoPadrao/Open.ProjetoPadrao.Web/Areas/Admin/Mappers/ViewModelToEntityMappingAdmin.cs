﻿using AutoMapper;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.ViewModels.Admin;

namespace Open.ProjetoPadrao.Web.Mappers.Admin
{
  public class ViewModelToEntityMappingAdmin : Profile
  {
    public ViewModelToEntityMappingAdmin()
    {
      CreateMap<UsuarioPerfilViewModel, UsuarioPerfil>();

      CreateMap<UsuarioViewModel, Usuario>()
        .ForMember(x => x.UsuarioPerfil, opt => opt.Ignore())
        .ForMember(x => x.Pessoa, opt => opt.Ignore());

      CreateMap<UsuarioViewModel, Pessoa>()
          .ForMember(a => a.NomeCompleto, b => b.MapFrom(src => src.Nome));

      CreateMap<VideoViewModel, Video>()
        .ForMember(a => a.CodigoTipoVideo, b => b.MapFrom(src => src.TipoVideo));

      CreateMap<BairroViewModel, Bairro>()
         .ForMember(a => a.Cidade, opt => opt.Ignore())
         .ForMember(a => a.Enderecos, opt => opt.Ignore());     
    }
  }
}