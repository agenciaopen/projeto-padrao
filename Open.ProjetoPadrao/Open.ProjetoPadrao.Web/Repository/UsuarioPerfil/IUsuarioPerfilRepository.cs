﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IUsuarioPerfilRepository : IRepositoryBase<UsuarioPerfil>
  {
    IEnumerable<UsuarioPerfil> BuscarPerfis(bool adicionarSuperUser = false);
    UsuarioPerfil BuscarPerfil(Guid idUsuarioPerfil);
    void RemoverPerfil(Guid idUsuarioPerfil);
    void CadastrarPerfil(UsuarioPerfil model);
    void EditarUsuarioPerfil(UsuarioPerfil model);
  }
}