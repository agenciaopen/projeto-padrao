﻿using System;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IPessoaRepository : IRepositoryBase<Pessoa>
  {
    void CadastrarPessoa(Pessoa model);
    Pessoa BuscarPessoaUsuario(Guid idUsuario);
    void EditarPessoa(Pessoa model);
  }
}