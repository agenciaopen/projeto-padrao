﻿using System;
using System.ComponentModel.DataAnnotations;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class HeadlineViewModel
  {
    [Key]
    public Guid IDPaginaControle { get; set; }

    [Required(ErrorMessage = "Informe o identificador do controle")]
    public string IdentificadorControle { get; set; }

    [Required(ErrorMessage = "Informe o conteúdo da headline")]
    public string ConteudoHeadline { get; set; }
  }
}