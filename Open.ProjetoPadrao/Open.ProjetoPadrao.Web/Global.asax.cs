﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Ninject;
using Ninject.Web.Common.WebHost;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.Authentication;

namespace Open.ProjetoPadrao.Web
{
  public class MvcApplication : NinjectHttpApplication
  {
    protected override IKernel CreateKernel()
    {
      var kernel = new StandardKernel();
      RegisterServices(kernel);
      return kernel;
    }

    private void RegisterServices(IKernel kernel)
    {
      kernel.Load(Assembly.GetExecutingAssembly());
      kernel.Bind<IUnitOfWork>().To<UnitOfWork.UnitOfWork>();
    }

    protected override void OnApplicationStarted()
    {
      base.OnApplicationStarted();

      ViewEngines.Engines.Clear();
      ViewEngines.Engines.Add(new RazorViewEngine());

      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
      AutoMapperConfig.RegisterMappings();
    }

    protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
    {
      var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

      if (authCookie != null)
      {
        var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        var serializer = new JavaScriptSerializer();
        var usuarioLogado = serializer.Deserialize<UsuarioSerializeModel>(authTicket.UserData);

        if (usuarioLogado != null)
        {
          UsuarioPrincipal model = new UsuarioPrincipal(authTicket.Name);

          model.IDUsuario = usuarioLogado.IDUsuario;
          model.IDUsuarioPerfil = usuarioLogado.IDUsuarioPerfil;
          model.IDPessoa = usuarioLogado.IDPessoa;
          model.Nome = usuarioLogado.Nome;
          model.Email = usuarioLogado.Email;
          model.SuperUser = usuarioLogado.SuperUser;
          model.CadastraPagina = usuarioLogado.CadastraPagina;
          model.EditaPagina = usuarioLogado.EditaPagina;
          model.Login = usuarioLogado.Login;
          model.Ativo = usuarioLogado.Ativo;
          model.Deletado = usuarioLogado.Deletado;
          model.AlterarSenhaPrimeiroAcesso = usuarioLogado.AlterarSenhaPrimeiroAcesso;
          model.ActionsPermitidas = usuarioLogado.ActionsPermitidas;

          HttpContext.Current.User = model;
        }
        else
        {
          Response.Cookies.Remove(Properties.Configs.NomeCookie);
          FormsAuthentication.SignOut();

          Server.ClearError();
          Response.Redirect("~/admin/entrar");
        }
      }
    }
  }
}