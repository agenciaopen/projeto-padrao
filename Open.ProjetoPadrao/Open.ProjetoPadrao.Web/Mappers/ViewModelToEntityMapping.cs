﻿using AutoMapper;
using Open.Framework.Extensoes;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Enums;
using Open.ProjetoPadrao.Web.ViewModels.Site;

namespace Open.ProjetoPadrao.Web.Mappers
{
  public class ViewModelToEntityMapping : Profile
  {
    public ViewModelToEntityMapping()
    {
      CreateMap<ViewModels.Admin.NewsletterViewModel, Newsletter>()
        .ForMember(a => a.Telefone, b => b.MapFrom(src => src.Telefone.RemoveNaoNumericos()));
      
      CreateMap<ContatoViewModel, Contato>()
        .ForMember(a => a.CodigoTipoContato, b => b.MapFrom(src => (int)TipoContato.Contato))
        .ForMember(a => a.Telefone, b => b.MapFrom(src => src.Telefone.RemoveNaoNumericos()));

      CreateMap<VideoViewModel, Video>()
      .ForMember(a => a.CodigoTipoVideo, b => b.MapFrom(src => src.TipoVideo));

      CreateMap<NewsLetterViewModel, Newsletter>()
        .ForMember(a => a.Telefone, a => a.MapFrom(src => string.Empty));
    }
  }
}