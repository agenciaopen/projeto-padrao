﻿using System;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class VideoViewModel
  {
    [Key, Required]
    public Guid IDVideo { get; set; }

    [Display(Name = "Título")]
    [Required(ErrorMessage = "Informe o título do vídeo"), MaxLength(255)]
    public string Titulo { get; set; }

    [MaxLength(500)]
    [Display(Name = "Descrição")]
    public string Descricao { get; set; }

    [Required(ErrorMessage = "Informe o Alt (SEO)"), MaxLength(500)]
    [Display(Name = "Alt (SEO)")]
    public string Alt { get; set; }

    [Required(ErrorMessage = "Informe o link do vídeo do YouTube"), MaxLength(500)]
    [RegularExpression(@"(https?://(www\.)?youtube\.com/.*v=\w+.*)|(https?://youtu\.be/\w+.*)|(.*src=.https?://(www\.)?youtube\.com/v/\w+.*)|(.*src=.https?://(www\.)?youtube\.com/embed/\w+.*)", ErrorMessage = "Informe um link do YouTube válido")]
    [Display(Name = "URL YouTube (Link)")]
    public string URL { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    [Display(Name = "Exibir na Home")]
    public bool? ExibirHome { get; set; }

    public DateTime DataCriacao { get; set; }

    [Display(Name = "Tipo do Vídeo")]
    [Required(ErrorMessage = "Selecione uma opção")]
    public int TipoVideo { get; set; }

    [NotMapped]
    public SelectList ListaExibirHome { get; set; }
    
    [NotMapped]
    public SelectList ListaTipoVideo { get; set; }
  }
}