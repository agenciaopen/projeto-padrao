﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class UsuarioPerfilViewModel
  {
    public UsuarioPerfilViewModel()
    {
      ListaMenusSelecionados = new List<Guid>();
      ListaMenus = new List<Menu>();
    }

    [Key]
    public Guid IDUsuarioPerfil { get; set; }

    [Required(ErrorMessage = "Informe o nome do perfil"), MaxLength(255)]
    public string Nome { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    public bool SuperUser { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    public bool? AcessoSistemaAdmin { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    public bool? CadastraPagina { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    public bool? EditaPagina { get; set; }

    [Required]
    public DateTime DataHoraCriacao { get; set; }

    public DateTime? DataHoraAlteracao { get; set; }

    public virtual ICollection<UsuarioPerfilMenu> UsuarioPerfilMenus { get; set; }

    [NotMapped]
    public virtual ICollection<Menu> ListaMenus { get; set; }

    [NotMapped]
    public List<Guid> ListaMenusSelecionados { get; set; }
    
    [NotMapped]
    public string ListaMenusString { get; set; }
  }
}