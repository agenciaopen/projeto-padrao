﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class NewsLetterViewModel
  {
    public string Nome { get; set; }
    public string Email { get; set; }
  }
}