﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface INewsletterRepository : IRepositoryBase<Newsletter>
  {
    void CadastrarNewsletter(Newsletter news);
    List<Newsletter> ListarNewsletters();
    Newsletter BuscarNewsletter(Guid idNewsletter);
    void RemoverNewsletter(Guid idNewsletter);
  }
}