﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IUsuarioRepository : IRepositoryBase<Usuario>
  {
    IEnumerable<Usuario> BuscarUsuarios();
    Usuario BuscarUsuario(string login);
    Usuario BuscarUsuario(Guid idUsuario);
    Usuario RetornaUsuarioPessoa(Guid idUsuario);
    void CadastrarUsuario(Usuario model);
    void EditarUsuario(Usuario model);
    void RemoverUsuario(Guid idUsuario);
    void AlterarSenhaUsuario(Guid idUsuario, string novaSenhaCriptografada);
  }
}