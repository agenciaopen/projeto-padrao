﻿using System.Web.Mvc;

namespace Open.ProjetoPadrao.Web.Areas.Admin
{
  public class AdminAreaRegistration : AreaRegistration
  {
    public override string AreaName
    {
      get
      {
        return "Admin";
      }
    }
 
    public override void RegisterArea(AreaRegistrationContext context)
    {
      context.Routes.LowercaseUrls = true;
 
      // Login
 
      context.MapRoute(
        "Entrar",
        "admin/entrar",
        new { controller = "Conta", action = "Entrar" }
      );

      context.MapRoute(
        "Nova Página",
        "admin/cadastros/paginas/nova-pagina",
        new { controller = "Cadastros", action = "NovaPagina" }
      );
      
      // Padrão

      context.MapRoute(
        "Default_Admin",
        "admin/{controller}/{action}/{id}",
        new { controller = "Principal", action = "Index", id = UrlParameter.Optional }
      );
    }
  }
}