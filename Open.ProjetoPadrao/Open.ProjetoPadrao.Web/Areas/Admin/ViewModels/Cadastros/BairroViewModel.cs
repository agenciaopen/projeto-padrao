﻿using System;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class BairroViewModel
  {
    [Required]
    public Guid IDBairro { get; set; }

    [Display(Name = "Estado (UF)")]
    [Required(ErrorMessage = "Selecione uma opção")]
    public Guid? IDEstado { get; set; }

    [Display(Name = "Cidade")]
    [Required(ErrorMessage = "Selecione uma opção")]
    public Guid? IDCidade { get; set; }

    [Required(ErrorMessage = "Informe o nome do bairro")]
    public string Nome { get; set; }

    [NotMapped]
    public SelectList ListaEstados { get; set; }

    [NotMapped]
    public SelectList ListaCidades { get; set; }
  }
}