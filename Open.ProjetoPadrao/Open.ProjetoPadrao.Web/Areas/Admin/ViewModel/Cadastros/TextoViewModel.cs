﻿using System;
using System.ComponentModel.DataAnnotations;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class TextoViewModel
  {
    [Key]
    public Guid IDPaginaControle { get; set; }

    [Required(ErrorMessage = "Informe o identificador do controle")]
    public string IdentificadorControle { get; set; }

    [Required(ErrorMessage = "Informe o conteúdo do texto")]
    public string ConteudoTexto { get; set; }
  }
}