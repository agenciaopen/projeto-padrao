﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IUsuarioPerfilMenuRepository : IRepositoryBase<UsuarioPerfilMenu>
  {
    IEnumerable<Menu> BuscarMenusPerfil(Guid idUsuarioPerfil);
    void RemoverMenusPerfil(Guid idUsuarioPerfil);
    void SalvarMenusPerfil(ICollection<UsuarioPerfilMenu> model);
  }
}