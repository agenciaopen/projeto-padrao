﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public sealed class ContatoGeralViewModel
  {
    public ContatoGeralViewModel()
    {
      IDContato = Guid.NewGuid();
    }

    public Guid IDContato { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
    public string Celular { get; set; }
    public string Assunto { get; set; }
    public string Mensagem { get; set; }
    public int Departamento { get; set; }
    public DateTime DataHoraInclusao { get; set; }
  }
}