﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IEstadoRepository : IRepositoryBase<Estado>
  {
    IEnumerable<Estado> BuscarEstados();
    Estado BuscarEstado(Guid IDEstado);
  }
}
