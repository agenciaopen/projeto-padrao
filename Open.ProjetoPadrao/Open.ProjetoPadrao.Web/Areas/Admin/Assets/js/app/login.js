﻿var erroExibido = false;

$(document).ready(function () {
  $("[data-id='txtCampoLogin']").keydown(function (s, e) {
    if (erroExibido)
      exibirErro(false);
  });
});

function inicioLogin() {
  $("#btnLoginAdmin").button('loading');
  exibirErro(false);
}

function sucessoLogin(retorno) {
  if (retorno.Erro) {
    $("#btnLoginAdmin").button('reset');
    exibirErro(retorno.Erro, retorno.Mensagem);
  }
  else
    window.location = retorno.UrlRetorno;
}

function falhaLogin(retorno) {
  $("#btnLoginAdmin").button('reset');
  exibirErro(retorno.Erro, retorno.Mensagem);
}

function exibirErro(exibir, mensagem) {
  if (exibir) {
    $("#btnLoginAdmin").prop("disabled", false);

    $("#divMensagemErroLogin").html(mensagem);
    $("#divMensagemErroLogin").show("normal");

    erroExibido = true;
  }
  else {
    $("#divMensagemErroLogin").hide("normal");
    erroExibido = false;
  }
}

window.onload = function () { $("#mostrarSenha").hide(); }

$("#txtSenha").on('change', function () {
  if ($("#txtSenha").val())
    $("#senhaBotao").show();
  else
    $("#senhaBotao").hide();
});

$(".mostrar").on('click', function () {
  var $pwd = $("#txtSenha");
  if ($pwd.attr('type') === 'password') {
    $pwd.attr('type', 'text');
  }
  else {
    $pwd.attr('type', 'password');
  }
});



(function ($) {
  "use strict";
  var input = $('.validate-input .input100');

  $('.validate-form').on('submit', function () {
    var check = true;

    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }

    return check;
  });


  $('.validate-form .input100').each(function () {
    $(this).focus(function () {
      hideValidate(this);
    });
  });

  function validate(input) {
    if ($(input).val().trim() == '') {
      return false;
    }
  }

  function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
  }

  function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
  }


})(jQuery);