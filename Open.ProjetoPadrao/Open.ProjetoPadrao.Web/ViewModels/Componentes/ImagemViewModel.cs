﻿namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class ImagemViewModel
  {
    public string Caminho { get; set; }
    public string Alt { get; set; }
    public bool Destaque { get; set; }
    public bool Interna { get; set; }
    public int OrdemExibicao { get; set; }
  }
}