﻿using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class MenuRepository : RepositoryBase<Menu>, IMenuRepository
  {
    private readonly Context _context;

    public MenuRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Menu> BuscarMenus()
    {
      return _context.Menus
        .OrderBy(m => m.IDMenuPai)
        .ThenBy(m => m.OrdemExibicao);
    }
  }
}