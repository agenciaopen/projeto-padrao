﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IVideoRepository : IRepositoryBase<Video>
  {
    IEnumerable<Video> ListarVideos(int? qtdRegistos, bool apenasHome = false);
    IEnumerable<Video> BuscarVideos();
    Video BuscarVideo(Guid idVideo);
    void CadastrarVideo(Video model);
    void EditarVideo(Video model);
    void RemoverVideo(Guid idVideo);
  }
}