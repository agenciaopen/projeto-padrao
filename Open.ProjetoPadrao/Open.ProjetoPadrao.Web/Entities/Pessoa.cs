﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
 [Table("Pessoa")]
 public class Pessoa
  {
    [Key]
    public Guid IDPessoa { get; set; }

    public char TipoPessoa { get; set; }

    [MaxLength(11)]
    public string CPF { get; set; }

    [MaxLength(14)]
    public string CNPJ { get; set; }

    [MaxLength(255)]
    public string RazaoSocial { get; set; }

    [MaxLength(255)]
    public string NomeFantasia { get; set; }

    [Required, MaxLength(255)]
    public string NomeCompleto { get; set; }

    [Required]
    public char Sexo { get; set; }

    [Required, MaxLength(255)]
    public string Email { get; set; }

    [MaxLength(255)]
    public string Telefone { get; set; }

    [Required]
    public DateTime DataHoraCriacao { get; set; }

    public DateTime? DataHoraAlteracao { get; set; }
  }
}