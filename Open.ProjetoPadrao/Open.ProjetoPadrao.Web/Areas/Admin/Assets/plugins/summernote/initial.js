﻿$('[data-tipo="summernote"]').summernote({
  lang: 'pt-BR',
  height: 200,
  callbacks: {
    // remove a formatação quando o conteúdo é colado no editor
    onPaste: function (e) {
      var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
      e.preventDefault();
      setTimeout(function () {
        document.execCommand('insertText', false, bufferText);
      }, 10);
    }
  }
});