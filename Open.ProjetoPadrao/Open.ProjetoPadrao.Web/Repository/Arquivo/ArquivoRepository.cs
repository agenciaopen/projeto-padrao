﻿using Open.ProjetoPadrao.Web.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class ArquivoRepository : RepositoryBase<Arquivo>, IArquivoRepository
  {
    private readonly Context _context;

    public ArquivoRepository(Context context) : base(context)
    {
      _context = context;
    }

    public string ObterCaminhoArquivo(Guid idArquivo)
    {
      Arquivo arquivo = this._context.Arquivos.Find(idArquivo);
      return arquivo.Caminho + arquivo.NomeReal;
    }

    private void ExcluirArquivoDiretorio(Arquivo arquivo)
    {
      string diretorioRaiz = Path.GetDirectoryName(HttpContext.Current.Server.MapPath("~/")) + "/" + arquivo.Caminho + arquivo.NomeReal;

      if (System.IO.File.Exists(diretorioRaiz))
      {
        try
        {
          System.IO.File.Delete(diretorioRaiz);
        }
        catch (System.IO.IOException e)
        {
          Console.WriteLine(e.Message);
          return;
        }
      }
    }

    public void ExcluirListaArquivosDiretorio(ICollection<Arquivo> listaArquivos)
    {
      foreach (Arquivo arquivo in listaArquivos)
      {
        this.ExcluirArquivoDiretorio(arquivo);
      }
    }

  }
}