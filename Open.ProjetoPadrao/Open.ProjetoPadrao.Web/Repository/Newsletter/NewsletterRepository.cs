﻿using System;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class NewsletterRepository : RepositoryBase<Newsletter>, INewsletterRepository
  {
    private readonly Context _context;

    public NewsletterRepository(Context context) : base(context)
    {
      _context = context;
    }

    public void CadastrarNewsletter(Newsletter news)
    {
      if (BuscarNewsletter(news.Email) == null)
      {
        news.IDNewsletter = Guid.NewGuid();
        news.DataCadastro = DateTime.Now;
        this._context.Newsletter.Add(news);
        this._context.SaveChanges();
      }
    }

    public Newsletter BuscarNewsletter(Guid idNewsletter)
    {
      return _context.Newsletter.Where(a => a.IDNewsletter.Equals(idNewsletter)).FirstOrDefault();
    }

    public Newsletter BuscarNewsletter(string email)
    {
      return _context.Newsletter.Where(a => a.Email.Trim().Equals(email.Trim())).FirstOrDefault();
    }

    public List<Newsletter> ListarNewsletters()
    {
      return this._context.Newsletter
        .OrderByDescending(s => s.DataCadastro)
        .ToList();
    }

    public void RemoverNewsletter(Guid idNewsletter)
    {
      var newsletter = _context.Newsletter
        .Where(a => a.IDNewsletter.Equals(idNewsletter))
        .FirstOrDefault();

      if (newsletter != null)
      {
        _context.Newsletter.Remove(newsletter);
        _context.SaveChanges();
      }
    }
  }
}