﻿$(document).on("click", "[data-nome='controle-pagina']", function () {
  var idControle = $(this).attr("data-idcontrole");
  var nomeControle = $(this).attr("data-nomecontrole");
  var urlAdicionar = $(this).parent().attr("data-urlAction");
  var idPagina = $(this).parent().attr("data-idpagina");

  swal({
    text: "Deseja inserir um controle do tipo " + nomeControle + "?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((adicionar) => {
      if (adicionar) {
        $.ajax({
          url: urlAdicionar,
          type: "POST",
          data: { idControle: idControle, idPagina: idPagina },
          success: function (controles) {
            $("#divControlesPagina").html(controles);
            recarregarDataTableControles();
          },
          error: function (xhr, status, error) {
            exibirAlerta("E", "Falha ao inserir o controle à página");
          }
        });
      }
    });
});
$(document).on("click", "#divNomePagina", function () {
  var $nomePagina = $("#nomePagina");
  var $txtEdicaoNomePagina = $("#txtEdicaoNomePagina");

  $nomePagina.addClass("hidden");
  $txtEdicaoNomePagina.removeClass("hidden");
  $txtEdicaoNomePagina.focus();
});
$(document).on("click", "[data-id='btnRemoverPagina']", function () {
  var $this = $(this);
  var idPagina = $this.data("idpagina");
  var nomePagina = $this.data("nomepagina");
  var urlRemover = $this.data("urlremover");

  swal({
    text: "Deseja realmente remover a página " + nomePagina + "?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {
        $this.button('loading');
        $.ajax({
          url: urlRemover,
          type: "POST",
          data: { idPagina: idPagina },
          success: function (paginas) {
            $this.button('reset');
            $("#divPaginas").html(paginas);
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirAlerta("E", "Falha ao remover a página");
          }
        });
      }
    });
});
$(document).on("keyup", "#txtEdicaoNomePagina", function () {
  $("#nomePagina").html($(this).val() + " - Clique para editar");
  $("#lblNomePagina").html($(this).val());
});
$(document).on("blur", "#txtEdicaoNomePagina", function () {
  var $nomePagina = $("#nomePagina");
  var $txtEdicaoNomePagina = $(this);

  if ($txtEdicaoNomePagina.val() != "") {
    $txtEdicaoNomePagina.prop("disabled", true);

    var urlSalvarPagina = $txtEdicaoNomePagina.attr("data-urlSalvarPagina");
    var idPagina = $txtEdicaoNomePagina.attr("data-idpagina");

    $.ajax({
      url: urlSalvarPagina,
      type: "POST",
      data: { idPagina: idPagina, nomePagina: $txtEdicaoNomePagina.val() },
      success: function () {
        $txtEdicaoNomePagina.addClass("hidden");
        $nomePagina.removeClass("hidden");
        $txtEdicaoNomePagina.prop("disabled", false);
        $txtEdicaoNomePagina.val("");

        exibirMensagem("S", "Nome da página salvo com sucesso");
      },
      error: function (xhr, status, error) {
        $txtEdicaoNomePagina.prop("disabled", false);
        exibirAlerta("E", "Falha ao salvar os detalhes da página");
      }
    });
  }
});
$(document).on("click", "[data-id='btnDeletarControlePagina']", function () {
  var $this = $(this);
  var urlRemover = $this.data("url");
  var idPaginaControle = $this.data("idpaginacontrole");

  swal({
    text: "Deseja realmente deletar este controle?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {
        $this.button('loading');
        $.ajax({
          url: urlRemover,
          type: "POST",
          data: { idPaginaControle: idPaginaControle },
          success: function (controles) {
            $this.button('reset');
            $("#divControlesPagina").html(controles);
            recarregarDataTableControles();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirAlerta("E", "Falha ao remover o controle");
          }
        });
      }
    });
});

function recarregarDataTableControles() {
  $("#tabelaPaginaControles").DataTable({
    language: {
      processing: "Carregando...",
      search: "Pesquisar&nbsp;:",
      lengthMenu: "Exibir _MENU_ Itens",
      info: "Exibindo de _START_ at&eacute; _END_ de _TOTAL_ itens",
      infoEmpty: "Nenhum item encontrado",
      infoFiltered: "(Filtrado de _MAX_ no total)",
      infoPostFix: "",
      loadingRecords: "Carregando...",
      zeroRecords: "Nenhum item encontrado",
      emptyTable: "Nenhum item encontrado",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Pr&oacute;ximo",
        last: "&Uacute;ltimo"
      },
      aria: {
        sortAscending: ": organiza por ordem crescente",
        sortDescending: ": organiza por ordem decrescente"
      }
    }
  });
}