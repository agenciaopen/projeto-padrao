﻿using System;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;

namespace Open.ProjetoPadrao.Web.Attributes
{
  public class LocalizationAttribute : ActionFilterAttribute
  {
    private readonly string _idiomaPadrao;

    public LocalizationAttribute(string idiomaPadrao)
    {
      _idiomaPadrao = idiomaPadrao;
    }

    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      string idioma = (string)filterContext.RouteData.Values["idioma"] ?? _idiomaPadrao;

      try
      {
        Thread.CurrentThread.CurrentCulture =
          Thread.CurrentThread.CurrentUICulture = new CultureInfo(idioma);
      }
      catch (Exception)
      {
        throw new NotSupportedException($"Código de idioma inválido: '{idioma}'.");
      }
    }
  }
}