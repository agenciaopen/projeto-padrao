﻿using AutoMapper;
using Newtonsoft.Json;
using Open.Framework.Classes;
using Open.Framework.Uteis;
using Open.Framework.Enums;
using Open.Framework.Extensoes;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Enums;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.ViewModels.Admin;
using Open.ProjetoPadrao.Web.Properties;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class CadastrosController : BaseController
  {
    private Arquivos frameworkArquivo;

    public CadastrosController(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
      frameworkArquivo = new Arquivos();
    }

    #region Páginas

    [HttpGet]
    public ActionResult Paginas()
    {
      return View(BuscarPaginas());
    }

    [HttpGet]
    public ActionResult NovaPagina()
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("paginas"))
        return RedirectToAction("Index", "Principal");

      if (!UsuarioLogado.SuperUser && !UsuarioLogado.CadastraPagina)
        return RedirectToAction("Paginas");

      return RedirectToAction("Pagina", new { id = Guid.NewGuid() });
    }

    [HttpGet]
    public ActionResult Pagina(Guid id)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("paginas"))
        return RedirectToAction("Index", "Principal");

      var model = _unitOfWork.PaginaRepository.BuscarPagina(id);

      if (model == null)
      {
        model = new Pagina();

        model.IDPagina = id;
        model.Nome = "Página sem nome";
        model.PaginaControles = new List<PaginaControle>();

        _unitOfWork.PaginaRepository.CadastrarPagina(model);
      }

      var viewModel = Mapper.Map<Pagina, PaginaViewModel>(model);

      viewModel.ListaControles = _unitOfWork.ControleRepository
       .BuscarControles();

      return View(viewModel);
    }

    [HttpPost]
    public ActionResult RemoverPagina(Guid idPagina)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("paginas"))
        return Json(new RetornoJson(true, "Seu usuário não tem permissão para esta ação", string.Empty));

      if (!UsuarioLogado.SuperUser && !UsuarioLogado.EditaPagina)
        return RedirectToAction("Paginas");

      _unitOfWork.PaginaControleImagemRepository.RemoverControleImagens(idPagina);
      _unitOfWork.PaginaControleRepository.RemoverTodosControlesPagina(idPagina);
      _unitOfWork.PaginaRepository.RemoverPagina(idPagina);

      return PartialView("_PartialGridPaginas", BuscarPaginas());
    }

    [HttpPost]
    public void SalvarPagina(Guid idPagina, string nomePagina)
    {
      if ((UsuarioLogado.SuperUser || UsuarioLogado.EditaPagina) && UsuarioLogado.ActionsPermitidas.Contains("paginas"))
        _unitOfWork.PaginaRepository.SalvarPagina(idPagina, nomePagina);
    }

    private IEnumerable<PaginasViewModel> BuscarPaginas()
    {
      return Mapper
        .Map<IEnumerable<Pagina>, IEnumerable<PaginasViewModel>>(_unitOfWork.PaginaRepository.BuscarPaginas());
    }

    #endregion

    #region Controles

    [HttpPost]
    public ActionResult AdicionarControlePagina(Guid idControle, Guid idPagina)
    {
      if (!UsuarioLogado.ActionsPermitidas.Contains("paginas"))
        return RedirectToAction("Index", "Principal");

      var controlePagina = new PaginaControle()
      {
        IDPaginaControle = Guid.NewGuid(),
        IDPagina = idPagina,
        IDControle = idControle,
        Configurado = false
      };

      _unitOfWork.PaginaControleRepository
        .AdicionarControlePagina(controlePagina);

      var model = _unitOfWork.PaginaControleRepository
        .BuscarControlesPagina(idPagina);

      return PartialView("_PartialGridPaginaControles", model);
    }

    [HttpGet]
    public ActionResult ConfiguracaoControle(Guid id)
    {
      if (!UsuarioLogado.SuperUser && !UsuarioLogado.EditaPagina)
        return RedirectToAction("Paginas");

      var model = _unitOfWork.PaginaControleRepository
        .BuscarControlePagina(id);

      if (model.PaginaControleImagens == null)
        model.PaginaControleImagens = new List<PaginaControleImagem>();

      return View(model);
    }

    [HttpPost]
    public ActionResult DeletarControlePagina(Guid idPaginaControle)
    {
      if (!UsuarioLogado.SuperUser && !UsuarioLogado.CadastraPagina)
        return RedirectToAction("Paginas");

      Guid idPagina = _unitOfWork.PaginaControleRepository
        .RemoverControlePagina(idPaginaControle);

      var model = _unitOfWork.PaginaControleRepository
        .BuscarControlesPagina(idPagina);

      return PartialView("_PartialGridPaginaControles", model);
    }

    [HttpPost]
    public void ConfigurarHeadline(Guid idPaginaControle, string identificador, string textoHeadline, string textoHeadlineEN)
    {
      if (UsuarioLogado.SuperUser || UsuarioLogado.EditaPagina)
      {
        var listaParametros = new List<PaginaControleParametro>();
        listaParametros.Add(new PaginaControleParametro()
        {
          IDPaginaControleParametro = Guid.NewGuid(),
          IDPaginaControle = idPaginaControle,
          ConteudoHeadline = textoHeadline,
          ConteudoHeadlineEN = textoHeadlineEN
        });

        _unitOfWork.PaginaControleParametroRepository.RemoverTodosParametrosControle(idPaginaControle);

        if (!string.IsNullOrEmpty(identificador))
          _unitOfWork.PaginaControleRepository.AtualizarConfiguracaoControlePagina(idPaginaControle, identificador);

        SalvarPaginaControleParametros(listaParametros);
      }
    }

    [HttpPost, ValidateInput(false)]
    public void ConfigurarTexto(Guid idPaginaControle, string identificador, string conteudoTexto, string conteudoTextoEN)
    {
      if (UsuarioLogado.SuperUser || UsuarioLogado.EditaPagina)
      {
        var listaParametros = new List<PaginaControleParametro>();
        listaParametros.Add(new PaginaControleParametro()
        {
          IDPaginaControleParametro = Guid.NewGuid(),
          IDPaginaControle = idPaginaControle,
          ConteudoTexto = conteudoTexto,
          ConteudoTextoEN = conteudoTextoEN
        });

        _unitOfWork.PaginaControleParametroRepository.RemoverTodosParametrosControle(idPaginaControle);

        if (!string.IsNullOrEmpty(identificador))
          _unitOfWork.PaginaControleRepository.AtualizarConfiguracaoControlePagina(idPaginaControle, identificador);

        SalvarPaginaControleParametros(listaParametros);
      }
    }

    [HttpPost, ValidateInput(false)]
    public void ConfigurarTitleDescription(Guid idPaginaControle, string identificador, string conteudoTitle, string conteudoDescription, string conteudoTitleEN, string conteudoDescriptionEN)
    {
      if (UsuarioLogado.SuperUser || UsuarioLogado.EditaPagina)
      {
        var listaParametros = new List<PaginaControleParametro>();
        listaParametros.Add(new PaginaControleParametro()
        {
          IDPaginaControleParametro = Guid.NewGuid(),
          IDPaginaControle = idPaginaControle,
          ConteudoTitle = conteudoTitle,
          ConteudoDescription = conteudoDescription,
          ConteudoTitleEN = conteudoTitleEN,
          ConteudoDescriptionEN = conteudoDescriptionEN
        });

        _unitOfWork.PaginaControleParametroRepository.RemoverTodosParametrosControle(idPaginaControle);

        if (!string.IsNullOrEmpty(identificador))
          _unitOfWork.PaginaControleRepository.AtualizarConfiguracaoControlePagina(idPaginaControle, identificador);

        SalvarPaginaControleParametros(listaParametros);
      }
    }

    [HttpPost]
    public void EnviarImagemGaleria(Guid idPaginaControle)
    {
      var anexo = frameworkArquivo
        .PrepararArquivo("Imagens/Galeria", idPaginaControle, Request.Files[0], TipoArquivo.Imagem, TipoHospedagemArquivo.LOCAL);

      if (anexo != null)
      {
        var model = new PaginaControleImagem
        {
          Arquivo = anexo,
          IDArquivo = anexo.IDArquivo,
          IDPaginaControleImagem = Guid.NewGuid(),
          IDPaginaControle = idPaginaControle
        };

        _unitOfWork.PaginaControleImagemRepository.SalvarControleImagem(model);
      }
    }

    [HttpPost]
    public ActionResult RemoverImagemGaleria(Guid idPaginaControle, Guid idPaginaControleImagem)
    {
      var model = _unitOfWork.PaginaControleImagemRepository
        .BuscarPaginaControleImagem(idPaginaControleImagem);

      if (model != null)
      {
        string caminhoPastaGaleria = string.Format("{0}Uploads/Imagens/Galeria/{1}",
         AppDomain.CurrentDomain.BaseDirectory,
         idPaginaControle);

        string caminhoArquivo = string.Format("{0}/{1}{2}", caminhoPastaGaleria,
          model.Arquivo.NomeHash, model.Arquivo.Extensao);

        if (System.IO.File.Exists(caminhoArquivo))
          System.IO.File.Delete(caminhoArquivo);

        _unitOfWork.PaginaControleImagemRepository.RemoverPaginaControleImagem(model);
      }

      return BuscarImagensGaleria(idPaginaControle);
    }

    [HttpPost]
    public JsonResult SalvarImagemGaleria(Guid idPaginaControleImagem, string alt, string title, string legenda, string titulo, string subTitulo, string urlRedirecionamento, string altEN, string titleEN, string legendaEN, string tituloEN, string subTituloEN, string urlRedirecionamentoEN)
    {
      var retorno = new RetornoJson(false, "Imagem salva com sucesso", string.Empty);

      try
      {
        _unitOfWork.PaginaControleImagemRepository
          .SalvarDetalhesPaginaControleImagem(idPaginaControleImagem, alt, title, legenda, titulo, subTitulo, urlRedirecionamento, altEN, titleEN, legendaEN, tituloEN, subTituloEN, urlRedirecionamentoEN);
      }
      catch
      {
        retorno.Erro = true;
        retorno.Mensagem = "Ocorreu um erro ao salvar a imagem";
      }

      return Json(retorno);
    }

    [HttpPost]
    public ActionResult BuscarImagensGaleria(Guid idPaginaControle)
    {
      var model = _unitOfWork.PaginaControleImagemRepository.BuscarPaginaControleImagens(idPaginaControle);

      return PartialView("_PartialGridGaleriaImagens", model);
    }

    [HttpPost]
    public JsonResult SalvarIdentificadorGaleria(Guid idPaginaControle, string identificadorControle)
    {
      var retorno = new RetornoJson(false, "Controle salvo com sucesso", string.Empty);

      try
      {
        if (!string.IsNullOrEmpty(identificadorControle))
          _unitOfWork.PaginaControleRepository.AtualizarConfiguracaoControlePagina(idPaginaControle, identificadorControle);
      }
      catch
      {
        retorno.Erro = true;
        retorno.Mensagem = "Ocorreu um erro ao salvar o controle";
      }

      return Json(retorno);
    }

    private void SalvarPaginaControleParametros(List<PaginaControleParametro> model)
    {
      _unitOfWork.PaginaControleParametroRepository.AdicionarParametrosControle(model);
    }

    [HttpPost]
    public void ReordenarGaleria(Guid idPaginaControleImagem, int novaOrdem)
    {
      _unitOfWork.PaginaControleImagemRepository.ReordenarImagens(idPaginaControleImagem, novaOrdem);
    }

    #endregion
    
    #region Bairros

    [HttpGet]
    public ActionResult Bairros()
    {
      return View(BuscarBairros());
    }

    [HttpGet]
    public ActionResult NovoBairro()
    {
      var viewModel = new BairroViewModel();

      viewModel.ListaEstados = new SelectList(_unitOfWork.EstadoRepository
        .BuscarEstados(), "IDEstado", "NomeCompleto");

      return View(viewModel);
    }

    [HttpGet]
    public ActionResult Bairro(Guid id)
    {
      var model = _unitOfWork.BairroRepository
        .BuscarBairro(id);

      var viewModel = Mapper.Map<Bairro, BairroViewModel>(model);

      viewModel.ListaEstados = new SelectList(_unitOfWork.EstadoRepository.BuscarEstados(), "IDEstado", "NomeCompleto", viewModel.IDEstado);
      viewModel.ListaCidades = new SelectList(_unitOfWork.CidadeRepository.BuscarCidades(viewModel.IDEstado.Value), "IDCidade", "Nome", viewModel.IDCidade);

      return View(viewModel);
    }

    [HttpPost]
    public ActionResult RemoverBairro(Guid idBairro)
    {
      _unitOfWork.BairroRepository.RemoverBairro(idBairro);

      return PartialView("_PartialGridBairros", BuscarBairros());
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult CadastrarBairro(BairroViewModel viewModel)
    {
      viewModel.IDBairro = Guid.NewGuid();

      var retorno = new RetornoJson(
        false,
        "Bairro cadastrado com sucesso!",
        Url.Action("Bairro", "Cadastros", new { id = viewModel.IDBairro }));

      if (ModelState.IsValid)
      {
        try
        {
          var model = Mapper.Map<BairroViewModel, Bairro>(viewModel);
          _unitOfWork.BairroRepository.CadastrarBairro(model);
        }
        catch
        {
          retorno.Erro = true;
          retorno.UrlRetorno = string.Empty;
          retorno.Mensagem = "Ocorreu um erro ao cadastrar o bairro";
        }
      }
      else
      {
        retorno.Erro = true;
        retorno.UrlRetorno = string.Empty;
        retorno.Mensagem = "Selecione todos os campos para prosseguir";
      }

      return Json(retorno);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult EditarBairro(BairroViewModel viewModel)
    {
      var retorno = new RetornoJson(false, "Bairro editado com sucesso!", string.Empty);

      if (ModelState.IsValid)
      {
        try
        {
          var model = Mapper.Map<BairroViewModel, Bairro>(viewModel);
          _unitOfWork.BairroRepository.EditarBairro(model.IDBairro, model.IDCidade.Value, model.Nome);
        }
        catch
        {
          retorno.Erro = true;
          retorno.Mensagem = "Ocorreu um erro ao editar o bairro";
        }
      }
      else
      {
        retorno.Erro = true;
        retorno.UrlRetorno = string.Empty;
        retorno.Mensagem = "Selecione todos os campos para prosseguir";
      }

      return Json(retorno);
    }

    private IEnumerable<BairrosViewModel> BuscarBairros()
    {
      return Mapper
        .Map<IEnumerable<Bairro>, IEnumerable<BairrosViewModel>>(_unitOfWork.BairroRepository.BuscarBairros());
    }

    #endregion

    #region Vídeos

    [HttpGet]
    public ActionResult Videos()
    {
      return View(BuscarVideos());
    }

    [HttpGet]
    public ActionResult NovoVideo()
    {
      var viewModel = new VideoViewModel();

      viewModel.ListaExibirHome = PreencherListaSimNao();
      viewModel.ListaTipoVideo = PreencherListaTipoVideo();

      return View(viewModel);
    }

    [HttpGet]
    public ActionResult Video(Guid id)
    {
      var model = _unitOfWork.VideoRepository
        .BuscarVideo(id);

      var viewModel = Mapper.Map<Video, VideoViewModel>(model);

      viewModel.ListaExibirHome = PreencherListaSimNao(viewModel.ExibirHome);
      viewModel.ListaTipoVideo = PreencherListaTipoVideo(viewModel.TipoVideo);

      return View(viewModel);
    }

    [HttpPost]
    public ActionResult RemoverVideo(Guid idVideo)
    {
      _unitOfWork.VideoRepository.RemoverVideo(idVideo);

      return PartialView("_PartialGridVideos", BuscarVideos());
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult CadastrarVideo(VideoViewModel viewModel)
    {
      viewModel.IDVideo = Guid.NewGuid();
      viewModel.DataCriacao = DateTime.Now;
      viewModel.URL = NormalizacaoUrlVideo(viewModel.URL);

      if (string.IsNullOrEmpty(viewModel.URL))
        return Json(new RetornoJson(true, "O link do vídeo não é um link do YouTube válido", string.Empty));

      var retorno = new RetornoJson(false, "Vídeo cadastrado com sucesso!", Url.Action("Videos", "Cadastros"));

      try
      {
        var model = Mapper.Map<VideoViewModel, Video>(viewModel);
        _unitOfWork.VideoRepository.CadastrarVideo(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.UrlRetorno = string.Empty;
        retorno.Mensagem = "Ocorreu um erro ao cadastrar o vídeo";
      }

      return Json(retorno);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult EditarVideo(VideoViewModel viewModel)
    {
      var retorno = new RetornoJson(false, "Vídeo editado com sucesso!", string.Empty);

      try
      {
        viewModel.URL = NormalizacaoUrlVideo(viewModel.URL);

        var model = Mapper.Map<VideoViewModel, Video>(viewModel);
        _unitOfWork.VideoRepository.EditarVideo(model);
      }
      catch
      {
        retorno.Erro = true;
        retorno.Mensagem = "Ocorreu um erro ao editar o vídeo";
      }

      return Json(retorno);
    }

    private IEnumerable<VideosViewModel> BuscarVideos()
    {
      return Mapper
        .Map<IEnumerable<Video>, IEnumerable<VideosViewModel>>(_unitOfWork.VideoRepository.BuscarVideos());
    }

    private string NormalizacaoUrlVideo(string Url)
    {
      if (!string.IsNullOrEmpty(Url))
      {
        var YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)");
        Match youtubeMatch = YoutubeVideoRegex.Match(Url);
        return youtubeMatch.Success ? "https://www.youtube.com/embed/" + youtubeMatch.Groups[1].Value : string.Empty;
      }
      return string.Empty;
    }

    private SelectList PreencherListaTipoVideo(int tipoSelecionado = 0)
    {
      var listaTipos = new List<SelectListItem>();
      var tiposDisponiveis = Enum.GetValues(typeof(TipoVideo));

      foreach (var tipo in tiposDisponiveis)
        listaTipos.Add(new SelectListItem() { Value = ((int)tipo).ToString(), Text = tipo.ToString() });

      return new SelectList(listaTipos, "Value", "Text", tipoSelecionado);
    }

    #endregion
  }
}