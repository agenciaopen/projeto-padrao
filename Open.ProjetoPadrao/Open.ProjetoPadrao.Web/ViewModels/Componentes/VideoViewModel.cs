﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class VideoViewModel
  {
    public string URL { get; set; }
    public string Titulo { get; set; }
    public string Descricao { get; set; }
    public string Alt { get; set; }
    public int TipoVideo { get; set; }

    internal object ToList()
    {
      throw new NotImplementedException();
    }
  }
}