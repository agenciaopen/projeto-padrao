﻿$(document).on("click", "[data-id='linkIdioma']", function (e) {

  var $this = $(this);
  var idioma = $this.data("idioma");

  $.ajax({
    url: $("#urlModificarIdiomaSite").val(),
    type: "POST",
    data: { idioma: idioma }
  });
});