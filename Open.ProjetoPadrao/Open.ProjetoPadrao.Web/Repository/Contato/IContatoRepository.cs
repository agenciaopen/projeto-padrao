﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IContatoRepository : IRepositoryBase<Contato>
  {
    void CadastrarContato(Contato contato);
    IEnumerable<Contato> BuscarContatos();
    IEnumerable<Contato> BuscarContatos(TipoContato tipo);
    void AtualizarContatoVisualizado(Guid idContato);
    Contato CarregarContato(Guid idContato);
    void RemoverContato(Guid idContato);
  }
}