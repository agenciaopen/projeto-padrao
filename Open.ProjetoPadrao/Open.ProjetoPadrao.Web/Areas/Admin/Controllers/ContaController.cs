﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using Open.Framework.Enums;
using Open.Framework.Classes;
using Open.Framework.Seguranca;
using Open.ProjetoPadrao.Web.Entities;
using Open.ProjetoPadrao.Web.Properties;
using Open.ProjetoPadrao.Web.UnitOfWork;
using Open.ProjetoPadrao.Web.Attributes;
using Open.ProjetoPadrao.Web.Authentication;
using Open.ProjetoPadrao.Web.ViewModels.Admin;

namespace Open.ProjetoPadrao.Web.Areas.Admin.Controllers
{
  public class ContaController : BaseController
  {
    public ContaController(IUnitOfWork unitOfWork) : base(unitOfWork) { }

    [HttpGet, NotAjax, AllowAnonymous]
    public ActionResult Entrar()
    {
      return View();
    }

    [HttpPost, AjaxOnly, AllowAnonymous, ValidateAntiForgeryToken]
    public JsonResult Entrar(LoginViewModel model)
    {
      var retorno = new RetornoJson(true, string.Empty, FormsAuthentication.LoginUrl);

      try
      {
        var usuario = _unitOfWork.UsuarioRepository.BuscarUsuario(model.Login);

        if (usuario != null)
        {
          if (usuario.Ativo)
          {
            if (usuario.UsuarioPerfil.AcessoSistemaAdmin)
            {
              if (Criptografia.VerificarValoresCriptografados(model.Senha, usuario.Senha, TipoCriptografia.BCrypt))
              {
                GravarCookie(usuario);

                retorno.Erro = false;
                retorno.UrlRetorno = FormsAuthentication.GetRedirectUrl(usuario.Pessoa.Email, false);
              }
              else
              {
                retorno.Mensagem = Mensagens.UsuarioSenhaInvalidos;
              }
            }
            else
            {
              retorno.Mensagem = Mensagens.AcessoNegadoAdmin;
            }
          }
          else
          {
            retorno.Mensagem = Mensagens.UsuarioInativo;
          }
        }
        else
        {
          retorno.Mensagem = Mensagens.UsuarioSenhaInvalidos;
        }
      }
      catch
      {
        retorno.Mensagem = Mensagens.ErroLogin;
      }

      return Json(retorno);
    }

    [HttpGet]
    public ActionResult MinhaConta()
    {      
      return View(new MinhaContaViewModel());
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult SalvarMinhaConta(MinhaContaViewModel model)
    {
      var retorno = new RetornoJson(true, "Ocorreu um erro ao alterar sua senha", string.Empty);

      try
      {
        var usuario = _unitOfWork.UsuarioRepository.BuscarUsuario(UsuarioLogado.Login);

        if (usuario != null)
        {
          if (Criptografia.VerificarValoresCriptografados(model.SenhaAtual, usuario.Senha, TipoCriptografia.BCrypt))
          {
            string senhaCriptografada = Criptografia.Criptografar(model.NovaSenha, TipoCriptografia.BCrypt);
            _unitOfWork.UsuarioRepository.AlterarSenhaUsuario(UsuarioLogado.IDUsuario, senhaCriptografada);

            retorno.Erro = false;
            retorno.Mensagem = "Sua senha foi alterada com sucesso!";
          }
          else
          {
            retorno.Mensagem = "Senha atual inválida";
          }
        }
      }
      catch { }

      return Json(retorno);
    }

    [HttpGet, NotAjax]
    public void Sair()
    {
      HttpContext.Response.Cookies.Remove(Properties.Configs.NomeCookie);
      Session["MenusPermitidos"] = null;

      FormsAuthentication.SignOut();
      FormsAuthentication.RedirectToLoginPage();
    }

    private void GravarCookie(Usuario usuarioLogado)
    {
      var model = new UsuarioSerializeModel();

      model.IDUsuario = usuarioLogado.IDUsuario;
      model.IDUsuarioPerfil = usuarioLogado.IDUsuarioPerfil;
      model.IDPessoa = usuarioLogado.IDPessoa;
      model.Nome = usuarioLogado.Pessoa.NomeCompleto;
      model.Email = usuarioLogado.Pessoa.Email;
      model.Login = usuarioLogado.Login;
      model.Ativo = usuarioLogado.Ativo;
      model.SuperUser = usuarioLogado.UsuarioPerfil.SuperUser;
      model.CadastraPagina = usuarioLogado.UsuarioPerfil.CadastraPagina;
      model.EditaPagina = usuarioLogado.UsuarioPerfil.EditaPagina;

      StringBuilder sbActions = new StringBuilder();
      string nomeAction = string.Empty;

      var usuarioPerfilMenus = usuarioLogado.UsuarioPerfil.UsuarioPerfilMenus;

      foreach (UsuarioPerfilMenu usuarioPerfilMenu in usuarioPerfilMenus)
      {
        if (usuarioPerfilMenu.Menu != null && !string.IsNullOrEmpty(usuarioPerfilMenu.Menu.Action) && !string.IsNullOrEmpty(usuarioPerfilMenu.Menu.Controller))
        {
          nomeAction = usuarioPerfilMenu.Menu.Action.Equals("Index") ? usuarioPerfilMenu.Menu.Controller : usuarioPerfilMenu.Menu.Action;
          sbActions.Append(nomeAction + ";");
        }

        if (usuarioPerfilMenu.Menu != null)
        {
          foreach (Menu menuFilho in usuarioPerfilMenu.Menu.MenusFilhos)
          {
            if (!string.IsNullOrEmpty(menuFilho.Action) && !string.IsNullOrEmpty(menuFilho.Controller))
            {
              nomeAction = menuFilho.Action.Equals("Index") ? menuFilho.Controller : menuFilho.Action;
              sbActions.Append(nomeAction + ";");
            }
          }
        }
      }

      model.ActionsPermitidas = sbActions.ToString().ToLower();

      string userData = new JavaScriptSerializer().Serialize(model);

      var authTicket = new FormsAuthenticationTicket(
               1,
               usuarioLogado.Pessoa.NomeCompleto,
               DateTime.Now,
               DateTime.Now.AddHours(8),
               false,
               userData,
               FormsAuthentication.FormsCookiePath);

      string encTicket = FormsAuthentication.Encrypt(authTicket);
      HttpCookie faCookie = new HttpCookie(Configs.NomeCookie, encTicket);
      HttpContext.Response.Cookies.Add(faCookie);
    }
  }
}