﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class PaginaControleRepository : RepositoryBase<PaginaControle>, IPaginaControleRepository
  {
    private readonly Context _context;

    public PaginaControleRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<PaginaControle> BuscarControlesPagina(Guid idPagina)
    {
      return _context.PaginasControles
        .Include(a => a.Controle)
        .Where(a => a.IDPagina.Equals(idPagina))
        .OrderBy(a => a.IDControle);
    }

    public void AdicionarControlePagina(PaginaControle model)
    {
      _context.PaginasControles.Add(model);
      _context.SaveChanges();
    }

    public void RemoverTodosControlesPagina(Guid idPagina)
    {
      var todosParametros = _context.PaginasControleParametros
        .Where(a => a.PaginaControle.IDPagina.Equals(idPagina));

      if (todosParametros != null)
        _context.PaginasControleParametros.RemoveRange(todosParametros);

      var todosControles = _context.PaginasControles
        .Where(a => a.IDPagina.Equals(idPagina));

      if (todosControles != null)
        _context.PaginasControles.RemoveRange(todosControles);

      _context.SaveChanges();
    }

    public void AtualizarConfiguracaoControlePagina(Guid idPaginaControle, string identificador)
    {
      var model = _context.PaginasControles
        .Where(a => a.IDPaginaControle.Equals(idPaginaControle))
        .FirstOrDefault();

      if (model != null)
      {
        model.NomeIdentificadorControle = identificador;
        model.Configurado = true;

        _context.SaveChanges();
      }
    }

    public PaginaControle BuscarControlePagina(Guid idPaginaControle)
    {
      return _context.PaginasControles
        .Where(a => a.IDPaginaControle.Equals(idPaginaControle))
        .FirstOrDefault();
    }

    public Guid RemoverControlePagina(Guid idPaginaControle)
    {
      PaginaControle controle = _context.PaginasControles
        .Where(a => a.IDPaginaControle.Equals(idPaginaControle)).FirstOrDefault();

      if (controle != null)
      {
        if (controle.PaginaControleImagens.Any())
        {
          var controleImagem = _context.PaginasControleImagens.Where(a => a.IDPaginaControle.Equals(idPaginaControle));
          _context.PaginasControleImagens.RemoveRange(controleImagem);
        }
        else
        {
          var controleParametros = _context.PaginasControleParametros.Where(a => a.IDPaginaControle.Equals(idPaginaControle));
          _context.PaginasControleParametros.RemoveRange(controleParametros);
        }
      }

      var model = BuscarControlePagina(idPaginaControle);

      if (model != null)
      {
        _context.PaginasControles.Remove(model);
        _context.SaveChanges();

        return model.IDPagina;
      }

      return Guid.Empty;
    }
  }
}