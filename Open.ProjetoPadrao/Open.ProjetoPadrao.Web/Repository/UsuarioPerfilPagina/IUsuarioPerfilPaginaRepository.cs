﻿using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IUsuarioPerfilPaginaRepository : IRepositoryBase<UsuarioPerfilPagina>
  {
    List<Pagina> BuscarPaginasPerfil();
  }
}