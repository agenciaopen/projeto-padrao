﻿namespace Open.ProjetoPadrao.Web.Enums
{
  public enum TipoEmail
  {
    Contato,
    Empreendimento,
    FaleConosco,
    Parceria,
    LigamosParaVoce,
    VendaSeuTerreno
  }
}