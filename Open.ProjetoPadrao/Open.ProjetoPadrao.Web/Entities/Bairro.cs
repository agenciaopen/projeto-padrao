﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Bairro")]
  public class Bairro
  {
    public Bairro()
    {
      this.Enderecos = new Collection<Endereco>();
    }

    [Key]
    public Guid IDBairro { get; set; }

    [Required]
    public Guid? IDCidade { get; set; }
    
    [Required, MaxLength(200)]
    public string Nome { get; set; }


    public virtual Cidade Cidade { get; set; }
    
    public virtual ICollection<Endereco> Enderecos { get; set; }
  }
}