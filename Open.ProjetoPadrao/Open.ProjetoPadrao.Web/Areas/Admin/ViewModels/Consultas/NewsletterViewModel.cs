﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public sealed class NewsletterViewModel
  {
    public Guid IDNewsletter { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
    public bool ReceberWhatsapp { get; set; }
    public bool ReceberTelefone { get; set; }
    public DateTime DataCadastro { get; set; }
  }
}