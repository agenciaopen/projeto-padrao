﻿$("[data-tipo='datatable']").DataTable({
  order: [],
  language: {
    processing: "Carregando...",
    search: "Pesquisar&nbsp;:",
    lengthMenu: "Exibir _MENU_ Itens",
    info: "Exibindo de _START_ at&eacute; _END_ de _TOTAL_ itens",
    infoEmpty: "Nenhum item encontrado",
    infoFiltered: "(Filtrado de _MAX_ no total)",
    infoPostFix: "",
    loadingRecords: "Carregando...",
    zeroRecords: "Nenhum item encontrado",
    emptyTable: "Nenhum item encontrado",
    paginate: {
      first: "Primeiro",
      previous: "Anterior",
      next: "Pr&oacute;ximo",
      last: "&Uacute;ltimo"
    },
    aria: {
      sortAscending: ": organiza por ordem crescente",
      sortDescending: ": organiza por ordem decrescente"
    }
  }
});