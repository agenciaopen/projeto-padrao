﻿using System.Web.Mvc;
using Open.ProjetoPadrao.Web.Attributes;

namespace Open.ProjetoPadrao.Web
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new LocalizationAttribute("pt-BR"), 0);
      filters.Add(new CustomAuthorizeAttribute());
      filters.Add(new HandleErrorAttribute());
    }
  }
}