﻿using System;
using Open.ProjetoPadrao.Web.Repository;

namespace Open.ProjetoPadrao.Web.UnitOfWork
{
  public class UnitOfWork : IUnitOfWork
  {
    // Padrões

    private readonly Context _context = new Context();
    public IMenuRepository MenuRepository { get; set; }
    public IArquivoRepository ArquivoRepository { get; set; }
    public IPessoaRepository PessoaRepository { get; set; }
    public IUsuarioRepository UsuarioRepository { get; set; }
    public IUsuarioPerfilRepository UsuarioPerfilRepository { get; set; }
    public IUsuarioPerfilMenuRepository UsuarioPerfilMenuRepository { get; set; }
    public IUsuarioPerfilPaginaRepository UsuarioPerfilPaginaRepository { get; set; }
    public IPaginaControleRepository PaginaControleRepository { get; set; }
    public IPaginaControleImagemRepository PaginaControleImagemRepository { get; set; }
    public IPaginaControleParametroRepository PaginaControleParametroRepository { get; set; }
    public IPaginaRepository PaginaRepository { get; set; }
    public IControleRepository ControleRepository { get; set; }
    public IContatoRepository ContatoRepository { get; set; }
    public IContatoAnexoRepository ContatoAnexoRepository { get; set; }
    public IEstadoRepository EstadoRepository { get; set; }
    public ICidadeRepository CidadeRepository { get; set; }
    public INewsletterRepository NewsletterRepository { get; set; }
    public IEnderecoRepository EnderecoRepository { get; set; }
    public IBairroRepository BairroRepository { get; set; }
    public IVideoRepository VideoRepository { get; set; }
    public IRedirecionamentoSEORepository RedirecionamentoSEORepository { get; set; }

    // Customizados do Cliente

    // Aqui vão os mapeamentos customizados do projeto do cliente

    public UnitOfWork()
    {
      // Padrões

      MenuRepository = new MenuRepository(_context);
      ArquivoRepository = new ArquivoRepository(_context);
      PessoaRepository = new PessoaRepository(_context);
      UsuarioRepository = new UsuarioRepository(_context);
      UsuarioPerfilRepository = new UsuarioPerfilRepository(_context);
      UsuarioPerfilMenuRepository = new UsuarioPerfilMenuRepository(_context);
      UsuarioPerfilPaginaRepository = new UsuarioPerfilPaginaRepository(_context);
      PaginaControleRepository = new PaginaControleRepository(_context);
      PaginaControleImagemRepository = new PaginaControleImagemRepository(_context);
      PaginaControleParametroRepository = new PaginaControleParametroRepository(_context);
      PaginaRepository = new PaginaRepository(_context);
      ControleRepository = new ControleRepository(_context);
      ContatoRepository = new ContatoRepository(_context);
      ContatoAnexoRepository = new ContatoAnexoRepository(_context);
      EstadoRepository = new EstadoRepository(_context);
      CidadeRepository = new CidadeRepository(_context);
      NewsletterRepository = new NewsletterRepository(_context);
      EnderecoRepository = new EnderecoRepository(_context);
      BairroRepository = new BairroRepository(_context);
      VideoRepository = new VideoRepository(_context);
      RedirecionamentoSEORepository = new RedirecionamentoSEORepository(_context);

      // Customizados do Cliente

      // Aqui vão os mapeamentos customizados do projeto do cliente
    }

    private bool _disposed;
    
    public void Dispose()
    {
      Clear(true);
      GC.SuppressFinalize(this);
    }
    public void Commit()
    {
      _context.SaveChanges();
    }

    private void Clear(bool disposing)
    {
      if (!_disposed)
      {
        if (disposing)
        {
          _context.Dispose();
        }
      }
      _disposed = true;
    }

    ~UnitOfWork()
    {
      Clear(false);
    }
  }
}