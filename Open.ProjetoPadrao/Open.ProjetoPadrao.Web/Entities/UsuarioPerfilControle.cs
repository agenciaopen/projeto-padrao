﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("UsuarioPerfilControle")]
  public class UsuarioPerfilControle
  {
    [Key]
    public Guid IDUsuarioPerfilControle { get; set; }

    [Required]
    public Guid IDUsuarioPerfil { get; set; }

    [Required]
    public Guid IDControle { get; set; }

    public virtual Controle Controle { get; set; }
  }
}