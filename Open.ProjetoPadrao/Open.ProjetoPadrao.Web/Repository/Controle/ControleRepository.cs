﻿using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class ControleRepository : RepositoryBase<Controle>, IControleRepository
  {
    private readonly Context _context;

    public ControleRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Controle> BuscarControles()
    {
      return _context.Controles
        .OrderBy(a => a.Nome);
    }
  }
}