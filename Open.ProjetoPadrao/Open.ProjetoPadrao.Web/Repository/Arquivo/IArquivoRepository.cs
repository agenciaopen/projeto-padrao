﻿using Open.ProjetoPadrao.Web.Entities;
using System;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IArquivoRepository : IRepositoryBase<Arquivo>
  {
    string ObterCaminhoArquivo(Guid idArquivo);
  }
}
