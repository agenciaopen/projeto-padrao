﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Pagina")]
  public class Pagina
  {
    public Pagina()
    {
      PaginaControles = new List<PaginaControle>();
      ControlesPermitidosUsuario = new List<Controle>();
    }

    [Key]
    public Guid IDPagina { get; set; }

    [MaxLength(255)]
    public string Nome { get; set; }

    [MaxLength(50)]
    public string Identificador { get; set; }

    public virtual ICollection<PaginaControle> PaginaControles { get; set; }

    [NotMapped]
    public virtual List<Controle> ControlesPermitidosUsuario { get; set; }
  }
}