﻿using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IRedirecionamentoSEORepository : IRepositoryBase<RedirecionamentoSEO>
  {
    string BuscarURLNovaRedirecionamento(string url);
  }
}