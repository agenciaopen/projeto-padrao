﻿namespace Open.ProjetoPadrao.Web.Enums
{
  public enum TipoVideo
  {
    Destaque = 1,
    Entrega = 2,
    Lancamento = 3
  }
}