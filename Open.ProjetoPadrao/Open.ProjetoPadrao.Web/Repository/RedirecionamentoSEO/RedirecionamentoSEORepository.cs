﻿using System.Linq;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class RedirecionamentoSEORepository : RepositoryBase<RedirecionamentoSEO>, IRedirecionamentoSEORepository
  {
    private readonly Context _context;

    public RedirecionamentoSEORepository(Context context) : base(context)
    {
      _context = context;
    }

    public string BuscarURLNovaRedirecionamento(string url)
    {
      var redirecionamento = _context.RedirecionamentosSEO.FirstOrDefault(a => a.URLAntiga.Equals(url));

      if (redirecionamento == null)
        return string.Empty;
      else return redirecionamento.URLNova;
    }
  }
}