﻿using System.Data.Entity;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class Context : DbContext
  {
    #if DEBUG
      public Context() : base("name=Desemvolvimento") { Configuration.LazyLoadingEnabled = false; }
    #else
      public Context() : base("name=Homologacao") { Configuration.LazyLoadingEnabled = false; }
    #endif

    // Padrões

    public DbSet<Menu> Menus { get; set; }
    public DbSet<Pessoa> Pessoas { get; set; }
    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<UsuarioPerfil> UsuarioPerfis { get; set; }
    public DbSet<UsuarioPerfilPagina> UsuarioPerfilPaginas { get; set; }
    public DbSet<UsuarioPerfilMenu> UsuarioPerfilMenus { get; set; }
    public DbSet<PaginaControle> PaginasControles { get; set; }
    public DbSet<PaginaControleImagem> PaginasControleImagens { get; set; }
    public DbSet<PaginaControleParametro> PaginasControleParametros { get; set; }
    public DbSet<Pagina> Paginas { get; set; }
    public DbSet<Controle> Controles { get; set; }
    public DbSet<Arquivo> Arquivos { get; set; }
    public DbSet<Contato> Contatos { get; set; }
    public DbSet<ContatoAnexo> ContatoAnexos { get; set; }
    public DbSet<Estado> Estados { get; set; }
    public DbSet<Endereco> Enderecos { get; set; }
    public DbSet<Cidade> Cidades { get; set; }
    public DbSet<Newsletter> Newsletter { get; set; }
    public DbSet<Bairro> Bairros { get; set; }
    public DbSet<Video> Videos { get; set; }
    public DbSet<RedirecionamentoSEO> RedirecionamentosSEO { get; set; }
    
    // Customizados do Cliente

    // Aqui vão os mapeamentos customizados do projeto do cliente
  }
}