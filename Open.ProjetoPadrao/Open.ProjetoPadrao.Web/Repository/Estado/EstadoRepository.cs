﻿using Open.ProjetoPadrao.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class EstadoRepository : RepositoryBase<Estado>, IEstadoRepository
  {
    private readonly Context _context;

    public EstadoRepository(Context context) : base(context)
    {
      _context = context;
    }

    public Estado BuscarEstado(Guid IDEstado)
    {
      return _context.Estados
          .Where(e => e.IDEstado.Equals(IDEstado))
          .FirstOrDefault();
    }

    public IEnumerable<Estado> BuscarEstados()
    {
      return _context.Estados
        .OrderBy(s => s.NomeCompleto)
        .ToList();
    }
  }
}