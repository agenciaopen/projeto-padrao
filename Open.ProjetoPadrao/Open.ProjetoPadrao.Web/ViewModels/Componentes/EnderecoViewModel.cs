﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Open.ProjetoPadrao.Web.ViewModels.Site
{
  public sealed class EnderecoViewModel
  {
    public string Logradouro { get; set; }
    public string Numero { get; set; }
    public string Cidade { get; set; }
    public string Estado { get; set; }
    public string Bairro { get; set; }
  }
}