﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class PaginaRepository : RepositoryBase<Pagina>, IPaginaRepository
  {
    private readonly Context _context;

    public PaginaRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Pagina> BuscarPaginas()
    {
      return _context.Paginas
        .OrderBy(a => a.Nome);
    }

    public Pagina BuscarPagina(Guid idPagina)
    {
      return _context.Paginas
        .Where(a => a.IDPagina.Equals(idPagina))
        .Include(a => a.PaginaControles)
        .FirstOrDefault();
    }

    public Pagina BuscarPagina(string nomeIdentificador)
    {
      return _context.Paginas
                          .Where(s => s.Identificador.Equals(nomeIdentificador))
                          .FirstOrDefault();
    }

    public void CadastrarPagina(Pagina model)
    {
      _context.Paginas.Add(model);
      _context.SaveChanges();
    }

    public void SalvarPagina(Guid idPagina, string nomePagina)
    {
      var model = BuscarPagina(idPagina);

      if (model != null)
      {
        model.Nome = nomePagina;
        _context.SaveChanges();
      }
    }

    public void RemoverPagina(Guid idPagina)
    {
      var pagina = _context.Paginas
        .Where(a => a.IDPagina.Equals(idPagina))
        .FirstOrDefault();

      if (pagina != null)
      {
        _context.Paginas.Remove(pagina);
        _context.SaveChanges();
      }
    }
  }
}