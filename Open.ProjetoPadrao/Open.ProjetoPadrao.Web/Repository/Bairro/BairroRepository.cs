﻿using System;
using System.Linq;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class BairroRepository : RepositoryBase<Bairro>, IBairroRepository
  {
    private readonly Context _context;

    public BairroRepository(Context context) : base(context)
    {
      _context = context;
    }

    public IEnumerable<Bairro> BuscarBairros()
    {
      return Get().OrderBy(a => a.Nome);
    }
    public IEnumerable<Bairro> BuscarBairros(Guid idCidade)
    {
      return _context.Bairros
        .Where(s => s.IDCidade == idCidade)
        .OrderBy(s => s.Nome);
    }
    public Bairro BuscarBairro(Guid idBairro)
    {
      return Get(a => a.IDBairro.Equals(idBairro))
        .FirstOrDefault();
    }
    public void CadastrarBairro(Bairro model)
    {
      _context.Bairros.Add(model);
      _context.SaveChanges();
    }
    public void EditarBairro(Guid idBairro, Guid idCidade, string nome)
    {
      var model = _context.Bairros
        .FirstOrDefault(a => a.IDBairro.Equals(idBairro));

      if (model != null)
      {
        model.IDCidade = idCidade;
        model.Nome = nome;

        _context.SaveChanges();
      }
    }
    public void RemoverBairro(Guid idBairro)
    {
      var model = BuscarBairro(idBairro);

      if (model != null && model.Enderecos.Count == 0)
      {
        _context.Bairros.Remove(model);
        _context.SaveChanges();
      }
    }
  }
}