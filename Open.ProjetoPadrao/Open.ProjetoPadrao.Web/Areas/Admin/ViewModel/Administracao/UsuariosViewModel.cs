﻿using System;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class UsuariosViewModel
  {
    public Guid IDUsuario { get; set; }

    public string Nome { get; set; }
  
    public string Login { get; set; }

    public string Email { get; set; }

    public bool SuperUser { get; set; }

    public string NomePerfil { get; set; }
  }
}