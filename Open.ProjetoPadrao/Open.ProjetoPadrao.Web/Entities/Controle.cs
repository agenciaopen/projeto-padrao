﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Controle")]
  public class Controle
  {
    public Controle()
    {
      PaginaControles = new List<PaginaControle>();
    }

    [Key]
    public Guid IDControle { get; set; }

    [Required]
    public string Nome { get; set; }

    [Required]
    public string Descricao { get; set; }

    [Required]
    public string Icone { get; set; }

    [Required]
    public int CodigoTipoControle
    {
      get
      {
        return (int)TipoControle;
      }
      set
      {
        TipoControle = (TiposControle)value;
      }
    }

    [EnumDataType(typeof(TiposControle)), NotMapped]
    public TiposControle TipoControle { get; set; }

    public virtual ICollection<PaginaControle> PaginaControles { get; set; }
  }
}