﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("PaginaControleImagem")]
  public class PaginaControleImagem
  {
    [Key, Required]
    public Guid IDPaginaControleImagem { get; set; }

    [Required]
    public Guid IDPaginaControle { get; set; }

    [Required]
    public Guid IDArquivo { get; set; }

    public string Alt { get; set; }

    public string Title { get; set; }

    public string Legenda { get; set; }

    public string TextoTitulo { get; set; }

    public string TextoSubTitulo { get; set; }

    public string URLRedirecionamento { get; set; }

    public string AltEN { get; set; }

    public string TitleEN { get; set; }

    public string LegendaEN { get; set; }

    public string TextoTituloEN { get; set; }

    public string TextoSubTituloEN { get; set; }

    public string URLRedirecionamentoEN { get; set; }

    public int? OrdemExibicao { get; set; }

    [Required]
    public DateTime DataHoraInclusao { get; set; }

    public virtual PaginaControle PaginaControle { get; set; }

    public virtual Arquivo Arquivo { get; set; }
  }
}