﻿using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IControleRepository : IRepositoryBase<Controle>
  {
    IEnumerable<Controle> BuscarControles();
  }
}