﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.Entities
{
  [Table("Menu")]
  public class Menu
  {
    public Menu()
    {
      this.UsuarioPerfilMenus = new List<UsuarioPerfilMenu>();
    }

    [Key]
    public Guid IDMenu { get; set; }

    public Guid? IDMenuPai { get; set; }

    [Required, MaxLength(255)]
    public string TextoExibicao { get; set; }

    [Required, MaxLength(255)]
    public string Tooltip { get; set; }

    public string Icone { get; set; }

    public string Controller { get; set; }

    public string Action { get; set; }

    public short OrdemExibicao { get; set; }

    public virtual ICollection<UsuarioPerfilMenu> UsuarioPerfilMenus { get; set; }

    private List<Menu> _menusFilhos;

    [NotMapped]
    public List<Menu> MenusFilhos
    {
      get
      {
        if (_menusFilhos == null)
          _menusFilhos = new List<Menu>();

        return _menusFilhos;
      }
      set
      {
        _menusFilhos = value;
      }
    }
  }
}