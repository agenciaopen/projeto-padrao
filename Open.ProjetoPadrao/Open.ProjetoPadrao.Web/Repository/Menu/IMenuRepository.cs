﻿using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public interface IMenuRepository : IRepositoryBase<Menu>
  {
    IEnumerable<Menu> BuscarMenus();
  }
}