﻿using System;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class BairrosViewModel
  {
    public Guid IDBairro { get; set; }

    public string Nome { get; set; }

    public string NomeCidade { get; set; }

    public string NomeEstado { get; set; }

    public int QtdEnderecosVinculados { get; set; }
  }
}