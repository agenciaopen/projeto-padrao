﻿using System.Collections.Generic;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public sealed class PaginaGenericaViewModel
  {
    public IEnumerable<PaginaControleParametrosViewModel> PaginaControleParametros { get; set; }
  }
}