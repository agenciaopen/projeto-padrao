﻿using System;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Authentication
{
  public class UsuarioSerializeModel
  {
    public Guid IDUsuario { get; set; }
    public Guid IDUsuarioPerfil { get; set; }
    public Guid IDPessoa { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Login { get; set; }
    public bool Ativo { get; set; }
    public bool Deletado { get; set; }
    public bool AlterarSenhaPrimeiroAcesso { get; set; }
    public bool SuperUser { get; set; }
    public bool CadastraPagina { get; set; }
    public bool EditaPagina { get; set; }
    public DateTime DataHoraPrimeiroAcesso { get; set; }
    public List<Menu> MenusPermitidos { get; set; }
    public string ActionsPermitidas { get; set; }
  }
}