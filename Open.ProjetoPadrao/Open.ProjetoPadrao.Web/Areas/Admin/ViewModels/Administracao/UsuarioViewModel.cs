﻿using System;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class UsuarioViewModel
  {
    [Key, Required]
    public Guid IDUsuario { get; set; }

    [Required(ErrorMessage = "Selecione um Perfil")]
    [Display(Name = "Perfil")]
    public Guid IDUsuarioPerfil { get; set; }

    [Required]
    public Guid IDPessoa { get; set; }

    [Required(ErrorMessage = "Informe o nome")]
    [Display(Name = "Titulo Completo")]
    public string Nome { get; set; }

    [Required(ErrorMessage = "Informe o login")]
    public string Login { get; set; }

    [Required(ErrorMessage = "Informe o e-mail")]
    [DataType(DataType.EmailAddress)]
    [Display(Name = "E-mail")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Informe a senha")]
    [StringLength(32, ErrorMessage = "A senha deve ter no mínimo 6 e no máximo 32 caracteres", MinimumLength = 6)]
    [DataType(DataType.Password)]
    public string Senha { get; set; }

    [Required(ErrorMessage = "Confirme a senha")]
    [StringLength(32, ErrorMessage = "A senha deve ter no mínimo 6 e no máximo 32 caracteres", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [System.ComponentModel.DataAnnotations.Compare("Senha", ErrorMessage = "As senhas não coincidem")]
    [Display(Name = "Confirme a Senha")]
    public string ConfirmacaoSenha { get; set; }

    [Required(ErrorMessage = "Selecione uma opção")]
    public bool? Ativo { get; set; }

    [Required]
    public DateTime DataHoraCriacao { get; set; }

    public DateTime? DataHoraAlteracao { get; set; }

    [NotMapped]
    public SelectList ListPerfis { get; set; }
  }
}