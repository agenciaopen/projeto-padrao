﻿using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class ContatoViewModel
  {
    public object ObjContato { get; set; }
    public string Origem { get; set; }
    public TipoContato TipoContato { get; set; }
  }
}