﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Open.ProjetoPadrao.Web.Entities;

namespace Open.ProjetoPadrao.Web.Repository
{
  public class CidadeRepository : RepositoryBase<Cidade>, ICidadeRepository
  {
    private readonly Context _context;

    public CidadeRepository(Context context) : base(context)
    {
      _context = context;
    }
    
    public IEnumerable<Cidade> BuscarCidades(Guid idEstado)
    {
      return _context.Cidades
        .Where(s => s.IDEstado.Equals(idEstado))
        .OrderBy(s => s.Nome)
        .ToList();
    }

    public Cidade ObterCidade(Guid idCidade)
    {
      return _context.Cidades
        .FirstOrDefault(a => a.IDCidade.Equals(idCidade));
    }
  }
}