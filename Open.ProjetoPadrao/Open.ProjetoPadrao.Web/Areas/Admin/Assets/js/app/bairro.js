﻿$(document).on("click", "[data-id='btnRemoverBairro']", function () {

  var $this = $(this);
  var idBairro = $this.data("idbairro");
  var url = $("#urlRemoverBairro").val();
  
  swal({
    text: "Deseja remover este bairro?",
    icon: "warning",
    buttons: ["Não", "Sim"]
  })
    .then((sim) => {
      if (sim) {

        $this.button('loading');

        $.ajax({
          url: url,
          type: "POST",
          data: { idBairro: idBairro },
          success: function (partial) {
            $this.button('reset');
            $("#divBairros").html(partial);
            carregarDataTables();
          },
          error: function (xhr, status, error) {
            $this.button('reset');
            exibirMensagem("E", "Ocorreu um erro ao remover o bairro");
          }
        });
      }
    });

});
$(document).on("change", "#IDEstado", function () {
  var idEstado = $(this).val();
  var urlBuscaCidades = $(this).data("url");

  $.ajax({
    url: urlBuscaCidades,
    type: "POST",
    data: { idEstado: idEstado },
    success: function (cidades) {
      $('#IDCidade').empty();

      var itens = '<option value="">-- Cidade --</option>';

      $(cidades).each(function (i, cidade) {
        itens += "<option value='" + cidade.Value + "'>" + cidade.Text + "</option>";
      });

      $('#IDCidade').html(itens);
    },
    error: function (xhr, status, error) {
      exibirAlerta("E", "Falha ao buscar as cidades do estado");
    }
  });
});