﻿using System.ComponentModel.DataAnnotations;

 namespace Open.ProjetoPadrao.Web.Areas.Admin.ViewModel
{
  public class LoginViewModel
  {
    [Required(ErrorMessage = "Informe o usuário")]
    [Display(Name = "Usuário")]
    public string Login { get; set; }

    [Required(ErrorMessage = "Informe a senha")]
    [DataType(DataType.Password)]
    [Display(Name = "Senha")]
    public string Senha { get; set; }
  }
}