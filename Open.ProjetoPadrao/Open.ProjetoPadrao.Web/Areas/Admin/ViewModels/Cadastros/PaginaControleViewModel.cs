﻿using System;
using Open.ProjetoPadrao.Web.Enums;

namespace Open.ProjetoPadrao.Web.ViewModels.Admin
{
  public class PaginaControleViewModel
  {
    public Guid IDPaginaControle { get; set; }

    public Guid IDPagina { get; set; }

    public string NomePagina { get; set; }

    public string NomeControle { get; set; }

    public string IdentificadorControle { get; set; }
    
    public string Icone { get; set; }

    public string Descricao { get; set; }

    public string TextoConfigurado { get; set; }

    public string TextoBotao { get; set; }

    public string ClasseBotao { get; set; }

    public TiposControle TipoControle { get; set; }
  }
}